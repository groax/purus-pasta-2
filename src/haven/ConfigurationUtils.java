package haven;

import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import haven.purus.checklistbox.ChecklistBoxItem;
import org.json.JSONArray;

public class ConfigurationUtils {

    public static void loadPreferenceChecklist(String preferenceName, Map<String, ChecklistBoxItem> data) {
        try {
            String json = Utils.getpref(preferenceName, null);
            if (json == null) {
                return;
            }
            for (ChecklistBoxItem itm : data.values()) {
                itm.setSelected(false);
            }

            JSONArray jsonArray = new JSONArray(json);
            for (int i = 0; i < jsonArray.length(); i++) {
                ChecklistBoxItem itm = data.get(jsonArray.getString(i));
                if (itm != null) {
                    itm.setSelected(true);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void setPreferenceChecklist(String preferenceName, Map<String, ChecklistBoxItem> val) {
        try {
            String jsonArray = val.entrySet().stream().filter(entry -> entry.getValue().isSelected()).map(Entry::getKey).map(name -> "\"" + name + "\"")
                                  .collect(Collectors.joining(",", "[", "]"));

            Utils.setpref(preferenceName, jsonArray);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
