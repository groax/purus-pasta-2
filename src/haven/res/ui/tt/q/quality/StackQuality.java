package haven.res.ui.tt.q.quality;

import java.awt.*;

import haven.GOut;
import haven.Tex;
import haven.purus.Config;

public class StackQuality extends Quality {

    public StackQuality(Owner owner, double q) {
        super(owner, q);
    }

    @Override
    public void drawoverlay(GOut g, Tex ol) {
        if (Config.displayQuality.val) {
            super.drawoverlay(g, ol);
        }
    }

    @Override
    protected Color getOverlayColor() {
        return new Color(0, 65, 130, 150);
    }

}
