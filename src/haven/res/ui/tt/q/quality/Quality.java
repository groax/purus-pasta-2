/* Preprocessed source code */
package haven.res.ui.tt.q.quality;

/* $use: ui/tt/q/qbuff */
import java.awt.*;
import java.awt.image.BufferedImage;

import haven.*;
import haven.purus.Config;
import haven.res.ui.tt.q.qbuff.QBuff;

/* >tt: Quality */
@haven.FromResource(name = "ui/tt/q/quality", version = 26)
public class Quality extends QBuff implements GItem.OverlayInfo<Tex> {
    public static boolean show = Utils.getprefb("qtoggle", false);
    private static final BufferedImage icon = Resource.remote().loadwait("ui/tt/q/quality").layer(Resource.imgc, 0).scaled();
    public double q;

    public Quality(Owner owner, double q) {
        super(owner, icon, "Quality", q);
        this.q = q;
    }

	public static class Fac implements ItemInfo.InfoFactory {
		public Fac() {}

		@Override
		public ItemInfo build(Owner owner, Raw raw, Object... args) {
			return Quality.mkinfo(owner, args);
		}

	}

    public static ItemInfo mkinfo(Owner owner, Object... args) {
	return(new Quality(owner, ((Number)args[1]).doubleValue()));
    }

    public Tex overlay() {
		return(new TexI(GItem.NumberInfo.numrender((int)Math.round(q), new Color(255, 255, 255, 255))));
    }

    public void drawoverlay(GOut g, Tex ol) {
		if (show || Config.displayQuality.val) {
			g.chcolor(getOverlayColor());
			g.frect(g.sz().sub(g.sz().x, ol.sz().y), ol.sz());
			g.chcolor();
			g.aimage(ol, new Coord(0, g.sz().y), 0, 1);
		}
    }

	protected Color getOverlayColor() {
		return new Color(0, 0, 0, 150);
	}

}
