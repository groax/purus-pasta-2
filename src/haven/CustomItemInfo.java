package haven;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalDouble;

import haven.ItemInfo.Owner;
import haven.res.ui.stackinv.ItemStack;
import haven.res.ui.tt.q.quality.Quality;
import haven.res.ui.tt.q.quality.StackQuality;

public class CustomItemInfo {

    public static void apply(List<ItemInfo> ret, Owner owner) {
        if (owner instanceof GItem item) {
            applyForItem(ret, item);
        }
    }

    private static void applyForItem(List<ItemInfo> ret, GItem item) {
        if (item.contents instanceof ItemStack stack) {
            createStackQuality(stack, item).ifPresent(ret::add);
        }
    }

    private static Optional<StackQuality> createStackQuality(ItemStack stack, GItem owner) {
        OptionalDouble average = stack.order.stream()
                                            .map(item -> ItemInfo.find(Quality.class, item.info()))
                                            .filter(Objects::nonNull)
                                            .mapToDouble(quality -> quality.q)
                                            .average();
        if (average.isPresent()) {
            return Optional.of(new StackQuality(owner, average.getAsDouble()));
        }
        return Optional.empty();
    }

}
