package haven.purus.botdata;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import haven.purus.database.Database;
import haven.purus.foodhistory.FoodEntryDto;
import haven.purus.foodhistory.FoodHistoryState;

public class FoodHistoryData {

    public static void initFoodHistory() {
        try {
            Database.connection.prepareStatement("""
                                                         CREATE TABLE IF NOT EXISTS FoodHistory
                                                         (
                                                         character_name TEXT UNIQUE,
                                                         required_fep DOUBLE PRECISION,
                                                         current_fep DOUBLE PRECISION
                                                         )
                                                         """).execute();
            Database.connection.prepareStatement("""
                                                         CREATE TABLE IF NOT EXISTS FoodHistoryEntry
                                                         (
                                                         food_history_id INT,
                                                         food_name TEXT,
                                                         count INT,
                                                         UNIQUE(food_history_id, food_name)
                                                         )
                                                         """).execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static long upsertState(String characterName, double requiredFep, double currentFep) {
        try {
            String sql = """
                    INSERT INTO FoodHistory (character_name, required_fep, current_fep)
                    VALUES(?,?,?)
                    ON CONFLICT(character_name)
                    DO UPDATE SET required_fep=excluded.required_fep, current_fep=excluded.current_fep
                    """;
            PreparedStatement ps = Database.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, characterName);
            ps.setDouble(2, requiredFep);
            ps.setDouble(3, currentFep);
            ps.execute();

            ps = Database.connection.prepareStatement("SELECT ROWID FROM FoodHistory WHERE character_name = ?");
            ps.setString(1, characterName);
            ResultSet generatedKeys = ps.executeQuery();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return -1;
    }

    public static void upsertFoodEntry(long foodHistoryId, String foodName, int count) {
        try {
            String sql = """
                    INSERT INTO FoodHistoryEntry (food_history_id, food_name, count)
                    VALUES(?,?,?)
                    ON CONFLICT(food_history_id, food_name)
                    DO UPDATE SET count=excluded.count
                    """;
            PreparedStatement ps = Database.connection.prepareStatement(sql);
            ps.setLong(1, foodHistoryId);
            ps.setString(2, foodName);
            ps.setInt(3, count);
            ps.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static Optional<FoodHistoryState> loadState(String characterName) {
        try {
            String sql = """
                    SELECT ROWID, character_name, required_fep, current_fep
                    FROM FoodHistory
                    WHERE character_name = ?
                    """;
            PreparedStatement ps = Database.connection.prepareStatement(sql);
            ps.setString(1, characterName);

            ResultSet resultSet = ps.executeQuery();
            return extractStateResult(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return Optional.empty();
    }

    private static Optional<FoodHistoryState> extractStateResult(ResultSet resultSet) throws SQLException {
        if (!resultSet.next()) {
            return Optional.empty();
        }

        long id = resultSet.getLong(1);
        String name = resultSet.getString(2);
        double requiredFep = resultSet.getDouble(3);
        double currentFep = resultSet.getDouble(4);

        List<FoodEntryDto> foodEntryDtos = loadFoodEntries(id);
        Map<String, FoodEntryDto> foodEntries = foodEntryDtos.stream()
                                                             .collect(Collectors.toMap(FoodEntryDto::getName, Function.identity()));
        FoodHistoryState foodHistoryState = new FoodHistoryState(id, name, foodEntries, requiredFep, currentFep);
        return Optional.of(foodHistoryState);
    }

    public static List<FoodEntryDto> loadFoodEntries(long foodHistoryId) {
        try {
            String sql = """
                    SELECT food_name, count
                    FROM FoodHistoryEntry
                    WHERE food_history_id = ?
                    """;
            PreparedStatement ps = Database.connection.prepareStatement(sql);
            ps.setLong(1, foodHistoryId);

            ResultSet resultSet = ps.executeQuery();
            List<FoodEntryDto> results = new ArrayList<>();
            while (resultSet.next()) {
                String foodName = resultSet.getString(1);
                int count = resultSet.getInt(2);

                results.add(new FoodEntryDto(foodName, new HashMap<>(), count));
            }

            return results;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return new ArrayList<>();
    }

    public static void removeFoodEntries(long foodHistoryId) {
        try {
            String sql = """
                    DELETE FROM FoodHistoryEntry
                    WHERE food_history_id = ?
                    """;
            PreparedStatement ps = Database.connection.prepareStatement(sql);
            ps.setLong(1, foodHistoryId);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
