package haven.purus;

import java.util.Map;
import java.util.function.Function;

import haven.purus.FoodDrinkData.DrinkVessel;
import haven.purus.FoodDrinkData.FoodType;
import haven.purus.FoodDrinkData.TypeDrink;
import haven.purus.FoodDrinkData.VesselResource;

import static java.util.stream.Collectors.toMap;

public record DrinkDataCache(FoodDrinkData data, Map<String, DrinkVessel> vessels, Map<String, FoodType> foods,
                      Map<String, TypeDrink> drinks, Map<String, VesselResource> vesselResources) {

    private DrinkDataCache(FoodDrinkData data) {
        this(data, data.drinkVessels().stream().collect(toMap(DrinkVessel::drink, Function.identity())),
             data.foodTypes().stream().collect(toMap(FoodType::name, Function.identity())),
             data.typeDrinks().stream().collect(toMap(TypeDrink::type, Function.identity())),
             data.vesselResources().stream().collect(toMap(VesselResource::vessel, Function.identity())));
    }

    public DrinkDataCache() {
        this(new FoodDrinkData());
    }

    public static DrinkDataCache initialize() {
        FoodDrinkData foodDrinkData = FoodDrinkData.loadData();
        return new DrinkDataCache(foodDrinkData);
    }

    public boolean containsFood(String foodName) {
        return foods.containsKey(foodName);
    }

}
