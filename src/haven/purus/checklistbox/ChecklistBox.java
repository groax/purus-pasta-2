package haven.purus.checklistbox;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import haven.GOut;
import haven.Listbox;
import haven.UI;

public class ChecklistBox extends Listbox<ChecklistBoxItem> {

    private final List<ChecklistBoxItem> listItems = new ArrayList<>();
    private final Runnable clickCallback;

    public ChecklistBox(int width, int rows, Runnable clickCallback) {
        this(width, UI.scale(15), rows, clickCallback);
    }

    public ChecklistBox(int width, int rowHeight, int rows, Runnable clickCallback) {
        super(width, rowHeight, rows);
        this.clickCallback = clickCallback;
    }

    public void addItem(ChecklistBoxItem item) {
        listItems.add(item);
        listItems.sort(ChecklistBoxItem::compareTo);
    }

    public void addItems(Collection<ChecklistBoxItem> items) {
        listItems.addAll(items);
        listItems.sort(ChecklistBoxItem::compareTo);
    }

    public void clearItems() {
        listItems.clear();
    }

    @Override
    protected void itemclick(ChecklistBoxItem item, int button) {
        if (button == 1) {
            item.click();
            super.itemclick(item, button);
            clickCallback.run();
        }
    }

    @Override
    protected ChecklistBoxItem listitem(int i) {
        return listItems.get(i);
    }

    @Override
    protected int listitems() {
        return listItems.size();
    }

    @Override
    protected void drawitem(GOut g, ChecklistBoxItem item, int i) {
        item.draw(g);
    }

}