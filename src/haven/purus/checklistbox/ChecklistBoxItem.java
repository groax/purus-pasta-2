package haven.purus.checklistbox;

import haven.CheckBox;

public class ChecklistBoxItem extends CheckBox implements Comparable<ChecklistBoxItem> {

    public ChecklistBoxItem(String name) {
        this(name, false);
    }

    public ChecklistBoxItem(String name, boolean value) {
        super(name, value);
    }

    public String getName() {
        return lbl.text;
    }

    public boolean isSelected() {
        return a;
    }

    public void setSelected(boolean selected) {
        set(selected);
    }

    @Override
    public int compareTo(ChecklistBoxItem o) {
        return this.getName().compareTo(o.getName());
    }

}
