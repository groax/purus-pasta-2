/* Preprocessed source code */
package haven.purus;

import java.awt.*;
import java.util.*;
import java.util.List;

import haven.*;
import haven.render.Homo3D;
import haven.render.Pipe;

public class DmgOverlay extends Sprite implements PView.Render2D {
	public static final Text.Foundry fnd = new Text.Foundry(Text.sans, 12);
	public static final KeyBinding kb_clearDamage = KeyBinding.get("clearDamage",
																   KeyMatch.forchar('D', KeyMatch.C | KeyMatch.S));
	private static final int OVERLAY_ID = 2777;

	Color[] colt = new Color[]{Color.RED, Color.YELLOW, Color.GREEN};
	Tex[] dmgt = new Tex[3];
	int[] dmg;
	long ownerId;
	Set<Integer> alreadyProcessedDamage = new HashSet<>();

	public static void initGob(Gob g) {
        Gob.Overlay ool = g.findol(OVERLAY_ID);
		if (ool == null && getDamageCache(g).containsKey(g.id)) {
			g.addol(new Gob.Overlay(g, new DmgOverlay(g, null), OVERLAY_ID));
		}
	}

	public static void clearAllDamage(GameUI gui) {
		if (gui == null) {
			return;
		}

		List<Long> gobIds = new ArrayList<>(gui.damageCache.keySet());
		for (Long id : gobIds) {
			if (id == null) {
				continue;
			}
			Gob gob = gui.ui.sess.glob.oc.getgob(id);
			clearDamage(gob);
			gui.damageCache.remove(id);
		}
	}

	public DmgOverlay(Gob owner, Resource res) {
		super(owner, res);
		ownerId = owner.id;
		dmg = getDamageCache(owner).computeIfAbsent(ownerId, (key) -> new int[3]);
		renderAllDamageText();
	}

	public void updDmg(int dmg, int type, int id) {
		if (!alreadyProcessedDamage.add(id)) {
			return;
		}
		this.dmg[type] += dmg;
		dmgt[type] = renderDamageText(type);
	}

	public void draw(GOut g, Pipe state) {
		Coord sc = Homo3D.obj2view(Coord3f.zu.add(0,0, 16), state, Area.sized(Coord.z, g.sz())).round2();
		if(sc == null)
			return;
		Coord c = Coord.z;
		for(int i=0; i<3; i++) {
			if(dmgt[i] != null) {
				if(c.x > 0)
					c.x += 2;
				c = c.add(dmgt[i].sz().x + 1, 0);
			}
		}
		c = c.div(-2);
		g.chcolor(new Color(0, 0, 0, 64));
		g.frect2(sc.add(c).sub(1,0), sc.add(c.inv()).add(1,16));
		g.chcolor();
		for(int i=0; i<3; i++) {
			if(dmgt[i] == null)
				continue;
			g.image(dmgt[i], sc.add(c));
			sc.x += dmgt[i].sz().x + 2;
		}
		g.chcolor();
	}

	private void renderAllDamageText() {
		for (int i = 0; i < 3; i++) {
			dmgt[i] = renderDamageText(i);
		}
	}

	private TexI renderDamageText(int type) {
		return new TexI(Utils.outline2(fnd.render(Integer.toString(this.dmg[type]), colt[type]).img,
									   Utils.contrast(colt[type])));
	}

	private static Map<Long, int[]> getDamageCache(Gob g) {
		return Optional.ofNullable(g)
					   .map(gob -> gob.glob)
					   .map(glob -> glob.sess)
					   .map(session -> session.ui)
					   .map(ui -> ui.gui)
					   .map(gameUI -> gameUI.damageCache)
					   .orElseGet(HashMap::new);
	}

	private static void clearDamage(Gob gob) {
		if (gob == null) {
			return;
		}
		Gob.Overlay ool = gob.findol(OVERLAY_ID);
		if (ool != null) {
			ool.remove(true);
		}
	}
}
