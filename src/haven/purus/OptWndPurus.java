package haven.purus;

import java.awt.*;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.prefs.BackingStoreException;

import haven.Button;
import haven.Label;
import haven.*;
import haven.purus.mapper.MapperConfiguration;
import haven.render.BaseColor;
import haven.render.Location;
import haven.render.Pipe;
import haven.render.States;

public class OptWndPurus extends BetterWindow {

	private TextEntry searchField;
	private EntryList el;
	private MapperConfiguration mapperConfiguration;
	private String currentSearchword = "";

	public OptWndPurus() {
		super(UI.scale(800, 800), "Pasta Options");

		searchField = add(new TextEntry(UI.scale(700), ""), UI.scale(50, 5));

		el = add(new EntryList(UI.scale(600, 750)), UI.scale(100, 25));

		Entry thingToggles = new Entry(new Label("Toggle things on login"), "Toggle on login");
		((Label)thingToggles.w).setcolor(Color.ORANGE);
		el.root.addSubentry(thingToggles);

		thingToggles.addSubentry(new Entry(new CheckBox("Toggle tracking on login"){
			{a = Config.toggleTracking.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.toggleTracking.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Toggle tracking on login"));
		thingToggles.addSubentry(new Entry(new CheckBox("Toggle criminal acts on login"){
			{a = Config.toggleCriminalacts.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.toggleCriminalacts.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Toggle criminal acts on login"));
		thingToggles.addSubentry(new Entry(new CheckBox("Toggle siege pointers on login"){
			{a = Config.toggleSiege.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.toggleSiege.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Toggle siege pointers on login"));
		Label runlbl;
		String[] speed = {"Crawling", "Walking", "Running", "Sprinting"};
		Entry speedOnloginLbl = new Entry(runlbl = new Label("Set movement speed on login: " + speed[Config.speedOnLogin.val]), "Set movement speed on login");
		thingToggles.addSubentry(speedOnloginLbl);
		speedOnloginLbl.addSubentry(new Entry(new HSlider(UI.scale(150), 0, 3, Config.speedOnLogin.val) {
			@Override
			public void changed() {
				Config.speedOnLogin.setVal(this.val);
				runlbl.settext("Set movement speed on login: " + speed[this.val]);
				super.changed();
			}
		}, ""));

		Entry uiSettings = new Entry(new Label("UI Settings"), "UI Settings");
		((Label)uiSettings.w).setcolor(Color.ORANGE);
		el.root.addSubentry(uiSettings);

		uiSettings.addSubentry(new Entry(new CheckBox("Use hardware cursor [Requires restart]"){
			{a = Config.hwcursor.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.hwcursor.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Use hardware cursor [Requires restart]"));


		uiSettings.addSubentry(new Entry(new CheckBox("Disable chat notification sound on party & village chat"){
			{a = Config.disableMultichatNotification.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.disableMultichatNotification.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Disable chat notification sound on party & village chat"));

		CheckBox kinStatusSfx = new CheckBox("Make sound for Kin online notification");
		kinStatusSfx.changed(Config.kinStatusSfx::setVal);
		kinStatusSfx.a = Config.kinStatusSfx.val;
		uiSettings.addSubentry(new Entry(kinStatusSfx, "Make sound for Kin online notification"));

		uiSettings.addSubentry(new Entry(new CheckBox("Disable fraktur font (ugly) [Requires restart]"){
			{a = Config.disableJorbfont.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.disableJorbfont.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Disable fraktur font [Requires restart]"));

		uiSettings.addSubentry(new Entry(new CheckBox("Disable session wnd"){
			{a = Config.disableSessWnd.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.disableSessWnd.setVal(!this.a);
				MultiSession.activeSession.root.multiSessionWindow.update();
				return super.mousedown(c, button);
			}
		}, "Disable session wnd"));

		Label fontScaleLbl = new Label("Font size scale [Requires restart]" + Config.fontScale.val + "x [Requires restart]");
		Entry fontScaleLblEntry = new Entry(fontScaleLbl, "Font size scale [Requires restart] font scaling");
		uiSettings.addSubentry(fontScaleLblEntry);
		fontScaleLblEntry.addSubentry(new Entry(new HSlider(UI.scale(400), 0, 100, Math.round(100 * (Config.fontScale.val - 1.0f))) {
			@Override
			public void changed() {
				Config.fontScale.setVal(this.val / 100.0f + 1);
				fontScaleLbl.settext("Font size scale: " + Config.fontScale.val + "x [Requires restart]");
				super.changed();
			}
		}, ""));

		uiSettings.addSubentry(new Entry(new CheckBox("Show gob damage numbers [Requires restart]"){
			{a = Config.showGobDecayNum.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.showGobDecayNum.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Show gob damage numbers [Requires restart]"));

		CheckBox showDamageFractures = new CheckBox("Show damage fractures [Requires restart]");
		showDamageFractures.a = Config.showDamageFractures.val;
		showDamageFractures.changed(Config.showDamageFractures::setVal);
		uiSettings.addSubentry(new Entry(showDamageFractures, "Show damage fractures [Requires restart]"));

		CheckBox showInventoryNumbers = new CheckBox("Show inventory numbers");
		showInventoryNumbers.a = Config.showInventoryNumber.val;
		showInventoryNumbers.changed(Config.showInventoryNumber::setVal);
		uiSettings.addSubentry(new Entry(showInventoryNumbers, "Show inventory numbers"));

		CheckBox menuShortcutsEnabled = new CheckBox("Enable menugrid hotkeys");
		menuShortcutsEnabled.changed(Config.menuShortcutsEnabled::setVal);
		menuShortcutsEnabled.a = Config.menuShortcutsEnabled.val;
		uiSettings.addSubentry(new Entry(menuShortcutsEnabled, "Enable menugrid hotkeys"));

		Label flwSpeed = new Label("Flowermenu speed: " + Config.flowermenuSpeed.val + "s");
		Entry flowerMenulbl = new Entry(flwSpeed, "Flower menu speed flowermenu");
		uiSettings.addSubentry(flowerMenulbl);
		flowerMenulbl.addSubentry(new Entry(new HSlider(UI.scale(400), 0, 100, Math.round(100 * Config.flowermenuSpeed.val)) {
			@Override
			public void changed() {
				Config.flowermenuSpeed.setVal(this.val / 100f);
				flwSpeed.settext("Flowermenu speed: " + Config.flowermenuSpeed.val + "s");
				super.changed();
			}
		}, ""));

		uiSettings.addSubentry(new Entry(new CheckBox("Show inventory on login") {
			{a = Config.invShowLogin.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.invShowLogin.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Show inventory on login"));

		uiSettings.addSubentry(new Entry(new CheckBox("Show belt on login") {
			{a = Config.beltShowLogin.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.beltShowLogin.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Show belt on login"));

		uiSettings.addSubentry(new Entry(new CheckBox("Show food history [Requires relog]") {
			{a = Config.showFoodHistory.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.showFoodHistory.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Show food history"));

		CheckBox barrelContent = new CheckBox("Show barrel content text");
		barrelContent.changed(Config.annotateBarrel::setVal);
		barrelContent.a = Config.annotateBarrel.val;
		uiSettings.addSubentry(new Entry(barrelContent, "Show barrel content text"));

		CheckBox showSpeed = new CheckBox("Show object speed");
		showSpeed.changed(Config.showSpeed::setVal);
		showSpeed.a = Config.showSpeed.val;
		uiSettings.addSubentry(new Entry(showSpeed, "Show object speed"));

		CheckBox reverseDrop = new CheckBox("Walk with item in hands by default");
		reverseDrop.changed(Config.reverseDropProtection::setVal);
		reverseDrop.a = Config.reverseDropProtection.val;
		uiSettings.addSubentry(new Entry(reverseDrop, "Walk with item in hands by default"));

		CheckBox showStackQuality = new CheckBox("Show stack quality");
		showStackQuality.changed(Config.displayStackQuality::setVal);
		showStackQuality.a = Config.displayStackQuality.val;
		uiSettings.addSubentry(new Entry(showStackQuality, "Show stack quality"));

		CheckBox foodVarietyIndicator = new CheckBox("Show food variety indicator [Requires food history]");
		foodVarietyIndicator.changed(Config.foodVarietyIndicator::setVal);
		foodVarietyIndicator.a = Config.foodVarietyIndicator.val;
		uiSettings.addSubentry(new Entry(foodVarietyIndicator, "Show food variety indicator"));

		CheckBox foodDrinkInfo = new CheckBox("Show food drink info");
		foodDrinkInfo.changed(Config.foodDrinkInfo::setVal);
		foodDrinkInfo.a = Config.foodDrinkInfo.val;
		uiSettings.addSubentry(new Entry(foodDrinkInfo, "Show food drink info"));

		CheckBox meterText = new CheckBox("Show text on meters");
		meterText.changed(Config.meterText::setVal);
		meterText.a = Config.meterText.val;
		uiSettings.addSubentry(new Entry(meterText, "Show text on meters"));

		Button resetWindows = new Button(UI.scale(400), "Reset windows positions (restart after clicking)", this::resetWindows);
		uiSettings.addSubentry(new Entry(resetWindows, "Reset windows positions (restart after clicking)"));

		Button clearDamage = new Button(UI.scale(400), "Clear combat damage counters", () -> DmgOverlay.clearAllDamage(ui.gui));
		uiSettings.addSubentry(new Entry(clearDamage, "Clear combat damage counters"));

		Entry cameraSettings = new Entry(new Label("Camera settings"), "Camera settings");
		((Label)cameraSettings.w).setcolor(Color.ORANGE);
		el.root.addSubentry(cameraSettings);

		Entry camScrollLbl = new Entry(new Label("Camera scroll zoom sensitivity"), "Camera scroll zoom sensitivity");
		cameraSettings.addSubentry(camScrollLbl);
		camScrollLbl.addSubentry(new Entry(new HSlider(UI.scale(400), 1, 200, Math.round(10 * Config.cameraScrollSensitivity.val)) {
			@Override
			public void changed() {
				Config.cameraScrollSensitivity.setVal(this.val / 10.0f);
				super.changed();
			}
		}, ""));

		cameraSettings.addSubentry(new Entry(new CheckBox("Bad Cam: Reverse X Axis"){
			{a = Config.reverseBadCamX.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.reverseBadCamX.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Bad Cam: Reverse X Axis"));

		cameraSettings.addSubentry(new Entry(new CheckBox("Bad Cam: Reverse Y Axis"){
			{a = Config.reverseBadCamY.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.reverseBadCamY.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Bad Cam: Reverse Y Axis"));

		var lockCamera = new CheckBox("Lock bad camera elevation");
		lockCamera.changed(Config.lockCameraElevation::setVal);
		lockCamera.a = Config.lockCameraElevation.val;
		cameraSettings.addSubentry(new Entry(lockCamera, "Lock bad camera elevation"));

		Entry displaySettings = new Entry(new Label("Display settings"), "Display settings");
		((Label)displaySettings.w).setcolor(Color.ORANGE);
		el.root.addSubentry(displaySettings);

		displaySettings.addSubentry(new Entry(new CheckBox("Hide visual flavor objects [Requires restart]"){
			{a = Config.flavorObjsVisual.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.flavorObjsVisual.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Hide visual flavor objects [Requires restart]"));

		displaySettings.addSubentry(new Entry(new CheckBox("Hide audio flavor objects [Requires restart]"){
			{a = Config.flavorObjsAudial.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.flavorObjsAudial.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Hide all flavor (including sound) objects [Requires restart]"));

		CheckBox hideWeatherVfx = new CheckBox("Hide weather flavor [Requires restart]");
		hideWeatherVfx.a = Config.flavorWeatherVfx.val;
		hideWeatherVfx.changed(value -> Config.flavorWeatherVfx.setVal(value));
		displaySettings.addSubentry(new Entry(hideWeatherVfx, "Hide weather flavor [Requires restart]"));

		CheckBox hideExperienceWindows = new CheckBox("Hide experience windows");
		hideExperienceWindows.a = Config.hideExperienceWindows.val;
		hideExperienceWindows.changed(value -> Config.hideExperienceWindows.setVal(value));
		displaySettings.addSubentry(new Entry(hideExperienceWindows, "Hide experience windows"));

		CheckBox hideTileTransitions = new CheckBox("Hide tile transitions [Requires refresh]");
		hideTileTransitions.a = Config.hideTileTransitions.val;
		hideTileTransitions.changed(value -> Config.hideTileTransitions.setVal(value));
		displaySettings.addSubentry(new Entry(hideTileTransitions, "Hide tile transitions [Requires refresh]"));

		CheckBox flatTerrain = new CheckBox("Flatten terrain");
		flatTerrain.a = Config.flatTerrain.val;
		flatTerrain.changed(value -> {
			Config.flatTerrain.setVal(value);
			Optional.ofNullable(ui)
					.map(u -> u.sess)
					.map(s -> s.glob)
					.map(g -> g.map)
					.ifPresent(MCache::refreshTerrainHeight);
		});
		displaySettings.addSubentry(new Entry(flatTerrain, "Flatten terrain"));

		displaySettings.addSubentry(new Entry(new CheckBox("Highlight tanning tubs and drying frames"){
			{a = Config.ttfHighlight.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.ttfHighlight.setVal(!this.a);
				refreshGobs();
				return super.mousedown(c, button);
			}
		}, "Highlight tanning tubs and drying frames"));


		displaySettings.addSubentry(new Entry(new CheckBox("Enable columns above players"){
			{a = Config.playerRadiuses.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.playerRadiuses.setVal(!this.a);
				refreshGobs();
				return super.mousedown(c, button);
			}
		}, "\"Enable columns above players"));

		Label cpScaleLbl = new Label("Cupboard height: " + Config.cupboardHeight.val + "x");
		Entry cpMenuLbl = new Entry(cpScaleLbl, "Cupboard height");
		displaySettings.addSubentry(cpMenuLbl);
		cpMenuLbl.addSubentry(new Entry(new HSlider(UI.scale(400), 1, 20, Math.round(20 * Config.cupboardHeight.val)) {
			@Override
			public void changed() {
				Config.cupboardHeight.setVal(this.val / 20f);
				SkelSprite.cupboardSize = new Location(new Matrix4f(1, 0, 0, 0,
						0, 1, 0, 0,
						0, 0, Config.cupboardHeight.val, 0,
						0, 0, 0, 1));
				resizeCupboards();
				cpScaleLbl.settext("Cupboard height: " + Config.cupboardHeight.val + "x");
				super.changed();
			}
		}, ""));

		Supplier<String> labelText = () -> "Palisade height: %sx [Requires view reload]".formatted(Config.palisadeHeight.val);
		Label palisadeScaleLbl = new Label(labelText.get());
		Entry palisadeMenuLbl = new Entry(palisadeScaleLbl, "Palisade height");
		displaySettings.addSubentry(palisadeMenuLbl);
		palisadeMenuLbl.addSubentry(new Entry(new HSlider(UI.scale(400), 1, 20, Math.round(20 * Config.palisadeHeight.val)) {
			@Override
			public void changed() {
				Config.palisadeHeight.setVal(this.val / 20f);
				StaticSprite.palisadeSize = new Location(new Matrix4f(1, 0, 0, 0,
						0, 1, 0, 0,
						0, 0, Config.palisadeHeight.val, 0,
						0, 0, 0, 1));
				resizeCupboards();
				palisadeScaleLbl.settext(labelText.get());
				super.changed();
			}
		}, ""));

		Entry combatSettings = new Entry(new Label("Combat Settings"), "Combat Settings");
		((Label)combatSettings.w).setcolor(Color.ORANGE);
		el.root.addSubentry(combatSettings);

		combatSettings.addSubentry(new Entry(new CheckBox("Proximity aggro kritters when clicked"){
			{a = Config.proximityKritterAggro.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.proximityKritterAggro.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Proximity aggro kritters when clicked"));

		combatSettings.addSubentry(new Entry(new CheckBox("Proximity aggro players when clicked"){
			{a = Config.proximityPlayerAggro.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.proximityPlayerAggro.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Proximity aggro players when clicked"));

		CheckBox simpleCombatOpenings = new CheckBox("Use simple combat openings");
		simpleCombatOpenings.changed(Config.simpleCombatOpenings::setVal);
		simpleCombatOpenings.a = Config.simpleCombatOpenings.val;
		combatSettings.addSubentry(new Entry(simpleCombatOpenings, "Use simple combat openings"));

		CheckBox moveCombatUi = new CheckBox("Enable combat ui moving");
		moveCombatUi.changed(Config.moveCombatUi::setVal);
		moveCombatUi.a = Config.moveCombatUi.val;
		combatSettings.addSubentry(new Entry(moveCombatUi, "Enable combat ui moving"));

		Button resetCombatUi = new Button(UI.scale(400), "Reset combat ui position",
										  () -> Fightsess.resetCombatUiPosition(ui.gui));
		combatSettings.addSubentry(new Entry(resetCombatUi, "Reset combat ui position"));

		Entry debugSettings = new Entry(new Label("Debug Settings"), "Debug Settings");
		((Label)debugSettings.w).setcolor(Color.ORANGE);
		el.root.addSubentry(debugSettings);

		debugSettings.addSubentry(new Entry(new CheckBox("Write resource source codes in debug directory"){
			{a = Config.debugRescode.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.debugRescode.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Write resource source codes in debug directory"));


		debugSettings.addSubentry(new Entry(new CheckBox("Print wdgmsg to console"){
			{a = Config.debugWdgmsg.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.debugWdgmsg.setVal(!this.a);
				return super.mousedown(c, button);
			}
		}, "Print wdgmsg to console"));

		Entry hideSettings = new Entry(new Label("Hide Settings"), "Hide Settings");
		((Label)hideSettings.w).setcolor(Color.ORANGE);
		el.root.addSubentry(hideSettings);

		el.root.addSubentry(new Entry(new Label("Toggle hide by ctrl + h (default keybinding)"), "Toggle hide by"));

		hideSettings.addSubentry(new Entry(new CheckBox("Hide trees"){
			{a = Config.hideTrees.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.hideTrees.setVal(!this.a);
				refreshHideSprites();
				refreshGobs();
				return super.mousedown(c, button);
			}
		}, "Hide trees"));

		hideSettings.addSubentry(new Entry(new CheckBox("Hide bushes"){
			{a = Config.hideBushes.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.hideBushes.setVal(!this.a);
				refreshHideSprites();
				refreshGobs();
				return super.mousedown(c, button);
			}
		}, "Hide bushes"));

		hideSettings.addSubentry(new Entry(new CheckBox("Hide crops"){
			{a = Config.hideCrops.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.hideCrops.setVal(!this.a);
				refreshHideSprites();
				refreshGobs();
				return super.mousedown(c, button);
			}
		}, "Hide crops"));

		hideSettings.addSubentry(new Entry(new CheckBox("Hide walls"){
			{a = Config.hideWalls.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.hideWalls.setVal(!this.a);
				refreshHideSprites();
				refreshGobs();
				return super.mousedown(c, button);
			}
		}, "Hide walls"));

		hideSettings.addSubentry(new Entry(new CheckBox("Hide houses"){
			{a = Config.hideHouses.val;}
			@Override
			public boolean mousedown(Coord c, int button) {
				Config.hideHouses.setVal(!this.a);
				refreshHideSprites();
				refreshGobs();
				return super.mousedown(c, button);
			}
		}, "Hide houses"));


		Entry hideRedLbl = new Entry(new Label("Hide red"), "Hide red");
		hideSettings.addSubentry(hideRedLbl);
		hideRedLbl.addSubentry(new Entry(new HSlider(UI.scale(400), 0, 255, Config.hideRed.val) {
			@Override
			public void changed() {
				Config.hideRed.setVal(this.val);
				GobHideBox.HidePol.emat = Pipe.Op.compose(new BaseColor(new java.awt.Color(Config.hideRed.val, Config.hideGreen.val, Config.hideBlue.val, Config.hideAlpha.val)), new States.LineWidth(3));
				refreshHideSprites();
				refreshGobs();
				super.changed();
			}
		}, ""));

		Entry hideGreenLbl = new Entry(new Label("Hide green"), "Hide green");
		hideSettings.addSubentry(hideGreenLbl);
		hideGreenLbl.addSubentry(new Entry(new HSlider(UI.scale(400), 0, 255, Config.hideGreen.val) {
			@Override
			public void changed() {
				Config.hideGreen.setVal(this.val);
				GobHideBox.HidePol.emat = Pipe.Op.compose(new BaseColor(new java.awt.Color(Config.hideRed.val, Config.hideGreen.val, Config.hideBlue.val, Config.hideAlpha.val)), new States.LineWidth(3));
				refreshHideSprites();
				refreshGobs();
				super.changed();
			}
		}, ""));

		Entry hideBlueLbl = new Entry(new Label("Hide blue"), "Hide blue");
		hideSettings.addSubentry(hideBlueLbl);
		hideBlueLbl.addSubentry(new Entry(new HSlider(UI.scale(400), 0, 255, Config.hideBlue.val) {
			@Override
			public void changed() {
				Config.hideBlue.setVal(this.val);
				GobHideBox.HidePol.emat = Pipe.Op.compose(new BaseColor(new java.awt.Color(Config.hideRed.val, Config.hideGreen.val, Config.hideBlue.val, Config.hideAlpha.val)), new States.LineWidth(3));
				refreshHideSprites();
				refreshGobs();
				super.changed();
			}
		}, ""));

		Entry hideAlphaLbl = new Entry(new Label("Hide alpha"), "Hide alpha");
		hideSettings.addSubentry(hideAlphaLbl);
		hideAlphaLbl.addSubentry(new Entry(new HSlider(UI.scale(400), 0, 255, Config.hideAlpha.val) {
			@Override
			public void changed() {
				Config.hideAlpha.setVal(this.val);
				GobHideBox.HidePol.emat = Pipe.Op.compose(new BaseColor(new java.awt.Color(Config.hideRed.val, Config.hideGreen.val, Config.hideBlue.val, Config.hideAlpha.val)), new States.LineWidth(3));
				refreshHideSprites();
				refreshGobs();
				super.changed();
			}
		}, ""));

		mapperConfiguration = new MapperConfiguration();
		el.root.addSubentry(mapperConfiguration.createConfigurationUI());

		el.search("");
	}

	private void resizeCupboards() {
		OCache cache = Optional.ofNullable(gameui())
							   .map(g -> g.ui)
							   .map(ui -> ui.sess)
							   .map(s -> s.glob)
							   .map(g -> g.oc)
							   .orElse(null);
		if (cache == null) {
			return;
		}

		for (Gob gob : cache) {
			try {
				Resource res = gob.getres();
				if (res != null && res.name.equals("gfx/terobjs/cupboard")) {
					gob.setattr(gob.getattr(ResDrawable.class));
				}
			} catch (Loading l) {
			}
		}
	}

	private void refreshHideSprites() {
		GameUI gameui = gameui();
		if (gameui == null || gameui.map == null) {
			return;
		}
		gameui.map.removeCustomSprites(1340);
	}

	private void refreshGobs() {
		GameUI gameui = gameui();
		if (gameui == null || gameui.map == null) {
			return;
		}
		gameui.map.refreshGobsAll();
	}

	private void resetWindows() {
		try {
			Arrays.stream(Config.pref.keys())
				  .filter(key -> key.startsWith("window#"))
				  .forEach(key -> Config.pref.remove(key));
		} catch (BackingStoreException e) {
			System.out.println("Failed to reset windows " + e);
			return;
		}
		System.out.println("Reset windows");
	}

	@Override
	public void draw(GOut g) {
		if(!currentSearchword.equals(searchField.buf.line())) {
			currentSearchword = searchField.buf.line();
			el.search(currentSearchword);
		}
		super.draw(g);
	}

	@Override
	public void wdgmsg(Widget sender, String msg, Object... args) {
		if(sender == this && msg.equals("close")) {
			hide();
		} else {
			super.wdgmsg(sender, msg, args);
		}
	}

}
