package haven.purus.automation.discovery;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.databind.ObjectMapper;
import haven.Coord2d;
import haven.purus.pbot.api.PBotFlowerMenu;
import haven.purus.pbot.api.PBotGob;
import haven.purus.pbot.api.PBotSession;
import haven.purus.pbot.external.HnhBot;

public class DiscoveryBot extends HnhBot {

    private final static String discoveredEntitiesFileSuffix = "_discovery.json";
    private final List<String> blacklist = Stream.of("oldstump", "stump").collect(Collectors.toList());

    private final ObjectMapper objectMapper;
    private final Pattern variationSuffixPattern;

    private PBotSession botSession;

    public DiscoveryBot() {
        this.objectMapper = new ObjectMapper();
        this.variationSuffixPattern = Pattern.compile("\\d+$");
    }

    @Override
    public void execute(PBotSession botSession) {
        this.botSession = botSession;
        botSession.PBotUtils().sysMsg("Starting discovery script");

        String characterName = botSession.PBotCharacterAPI().getCharacterName();
        if (characterName == null) {
            System.out.println("Character name is null");
            return;
        }

        List<DiscoveryItem> discoveryItems = loadAlreadyDiscoveredItems(characterName);
        Map<String, DiscoveryItem> alreadyDiscoveredItems = discoveryItems.stream().collect(Collectors.toMap(DiscoveryItem::name, Function.identity()));

        while (!isInterrupted) {
            discoverItems(characterName, alreadyDiscoveredItems);
        }

        botSession.PBotUtils().sysMsg("Finished discovery script");
    }

    private void discoverItems(String characterName, Map<String, DiscoveryItem> alreadyDiscoveredItems) {
        List<PBotGob> gobsInProximity = findGobsInProximity(alreadyDiscoveredItems);
        if (gobsInProximity.isEmpty()) {
            System.out.println("Gobs not found");
            isInterrupted = true;
            return;
        }

        PBotGob gobToBeDiscovered = gobsInProximity.get(0);
        PBotFlowerMenu gobFlowerMenu = openMenuOnGob(gobToBeDiscovered);
        if (gobFlowerMenu == null) {
            System.out.println("Flower menu open failed");
            isInterrupted = true;
            return;
        }

        List<String> correctOptions = findCorrectOptions(gobFlowerMenu);
        Optional<String> usedOption = correctOptions.stream().findAny().filter(option -> selectOption(gobFlowerMenu, option));

        if (!correctOptions.isEmpty() && !usedOption.isPresent()) {
            System.out.println("No option used but exists");
            isInterrupted = true;
            return;
        }

        gobFlowerMenu.closeMenu();

        updateDiscoveredItems(alreadyDiscoveredItems, gobToBeDiscovered, usedOption.orElse(""));
        saveAlreadyDiscoveredItems(characterName, alreadyDiscoveredItems.values());
        botSession.PBotCharacterAPI().cancelAct();
        botSession.PBotUtils().playerInventory().dropEverything();
    }

    private void updateDiscoveredItems(Map<String, DiscoveryItem> alreadyDiscoveredItems, PBotGob gobToBeDiscovered, String usedOption) {
        String resourceName = removeVariationSuffix(gobToBeDiscovered.getResname());
        Set<String> usedOptions = new HashSet<>();
        usedOptions.add(usedOption);
        DiscoveryItem item = new DiscoveryItem(resourceName, usedOptions);
        alreadyDiscoveredItems.put(resourceName, item);
    }

    private void saveAlreadyDiscoveredItems(String characterName, Collection<DiscoveryItem> discoveryItems) {
        File file = createFilePath(characterName);
        file.getParentFile().mkdirs();
        try {
            objectMapper.writeValue(file, discoveryItems);
        } catch (IOException e) {
            System.out.println("Discovered items serialization failed");
            e.printStackTrace();
        }
    }

    private List<DiscoveryItem> loadAlreadyDiscoveredItems(String characterName) {
        try {
            File file = createFilePath(characterName);
            if (!file.exists() || !file.canRead()) {
                return new ArrayList<>();
            }

            DiscoveryItem[] discoveryItems = objectMapper.readValue(new FileReader(file), DiscoveryItem[].class);
            return Arrays.asList(discoveryItems);
        } catch (IOException e) {
            System.out.println("Discovered items loading failed");
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    private List<PBotGob> findGobsInProximity(Map<String, DiscoveryItem> alreadyDiscoveredItems) {
        Set<String> discoveredResourceNames = alreadyDiscoveredItems.keySet();
        Pattern resourcePattern = Pattern.compile("gfx/terobjs/trees/.*|gfx/terobjs/bushes/.*|gfx/terobjs/bumlings/.*");
        Predicate<String> predicate = resourcePattern.asPredicate();

        List<PBotGob> foundGobs = botSession.PBotGobAPI().findGobsInPlayerProximity(1000.d);
        List<PBotGob> acceptedGobs = foundGobs.stream()
                                              .filter(gob -> gob.getResname() != null)
                                              .filter(gob -> predicate.test(gob.getResname()))
                                              .filter(gob -> blacklist.stream().noneMatch(suffix -> gob.getResname().endsWith(suffix)))
                                              .collect(Collectors.toList());
        List<PBotGob> notDiscoveredGobs = acceptedGobs.stream()
                                                      .filter(gob -> !discoveredResourceNames.contains(
                                                              removeVariationSuffix(gob.getResname())))
                                                      .collect(Collectors.toList());

        PBotGob player = botSession.PBotGobAPI().getPlayer();
        Coord2d playerCoords = player.getCoords();
        return notDiscoveredGobs.stream()
                                .sorted(Comparator.comparingDouble(o -> playerCoords.dist(o.getCoords())))
                                .collect(Collectors.toList());
    }

    private PBotFlowerMenu openMenuOnGob(PBotGob gobToBeDiscovered) {
        gobToBeDiscovered.pfClick(3, 0);
        return botSession.PBotUtils().getFlowermenu(15000);
    }

    private List<String> findCorrectOptions(PBotFlowerMenu menu) {
        Pattern resourcePattern = Pattern.compile("pick.*|chip.*");
        Predicate<String> predicate = resourcePattern.asPredicate();

        List<String> menuPetalNames = menu.getPetalNames();
        return menuPetalNames.stream().filter(name -> predicate.test(name.toLowerCase())).collect(Collectors.toList());
    }

    private boolean selectOption(PBotFlowerMenu menu, String option) {
        menu.choosePetal(option);

        boolean somethingChanged = botSession.PBotUtils().playerInventory().waitUntilInventoryChanges(20000);

        botSession.PBotCharacterAPI().cancelAct();
        return somethingChanged;
    }

    private File createFilePath(String characterName) {
        String filePath = getSafeCharacterName(characterName) + discoveredEntitiesFileSuffix;
        return new File(dataFolderPath, filePath);
    }

    private String getSafeCharacterName(String characterName) {
        try {
            return URLEncoder.encode(characterName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "default";
        }
    }

    private String removeVariationSuffix(String resourceName) {
        return variationSuffixPattern.matcher(resourceName).replaceFirst("");
    }

}
