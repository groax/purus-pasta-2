package haven.purus.automation.discovery;

import java.util.Set;

public record DiscoveryItem(String name, Set<String> selectedOptions) {

}
