package haven.purus.automation.exception;

public class EarlyFinishException extends BotException {

    public EarlyFinishException(String message) {
        super(message);
    }

}
