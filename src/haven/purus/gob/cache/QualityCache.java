package haven.purus.gob.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class QualityCache {

    public static final Pattern qualityPattern = Pattern.compile("\\d+$");
    public static final AtomicInteger lastCheckedGobId = new AtomicInteger();
    public static final Map<Integer, Integer> qualityByGobId = new ConcurrentHashMap<>();

}
