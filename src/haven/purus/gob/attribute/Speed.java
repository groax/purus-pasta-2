package haven.purus.gob.attribute;

import java.awt.*;

import haven.*;
import haven.Gob.Type;
import haven.purus.Config;
import haven.render.Homo3D;
import haven.render.Pipe;
import haven.render.RenderTree;

public class Speed extends GAttrib implements RenderTree.Node, PView.Render2D {

    private Tex speed;
    private double lspeed;

    public Speed(final Gob g) {
        super(g);
    }

    public static void processGob(Gob gob) {
        if (gob == null || gob.getattr(Speed.class) != null) {
            return;
        }

        if (gob.type != null && (gob.type == Type.PLAYER || gob.type.has(Type.CRITTER))) {
            Speed speedAttribute = new Speed(gob);
            gob.setattr(speedAttribute);
        }
    }

    @Override
    public void draw(GOut g, Pipe state) {
        if (!Config.showSpeed.val || speed == null) {
            return;
        }
        Coord sc = Homo3D.obj2view(new Coord3f(0, 0, 15), state, Area.sized(g.sz())).round2();
        if (sc.isect(Coord.z, g.sz())) {
            g.aimage(speed, sc, 0.5, 2.0);
        }
    }

    @Override
    public void ctick(double dt) {
        if (!Config.showSpeed.val) {
            return;
        }
        final double spd = gob.getv();
        if (spd != lspeed || speed == null) {
            speed = Text.std.renderstroked(String.format("%.2f", spd), new Color(255, 255, 255, 200), Color.black)
                            .tex();
            lspeed = spd;
        }
    }

    @Override
    public void dispose() {
        if (speed != null) {
            speed.dispose();
        }
    }

    @Override
    public String toString() {
        return "Speed(speed=" + lspeed + ")";
    }

}
