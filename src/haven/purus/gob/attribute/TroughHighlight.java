package haven.purus.gob.attribute;

import haven.GAttrib;
import haven.Gob;
import haven.Gob.Type;
import haven.ResDrawable;
import haven.render.MixColor;
import haven.render.Pipe;

public class TroughHighlight extends GAttrib implements Gob.SetupMod {

    private static final MixColor ttEmpty = new MixColor(255, 0, 0, 64);
    private static final MixColor ttDone = new MixColor(0, 255, 0, 64);
    private static final MixColor none = new MixColor(0, 0, 0, 0);

    public TroughHighlight(Gob g) {
        super(g);
    }

    public static void processGob(Gob gob) {
        if (gob == null || gob.type != Type.TROUGH || gob.getattr(TroughHighlight.class) != null) {
            return;
        }

        TroughHighlight highlightAttribute = new TroughHighlight(gob);
        gob.setattr(highlightAttribute);
    }

    @Override
    public Pipe.Op placestate() {
        ResDrawable rd = gob.getattr(ResDrawable.class);
        if (rd == null) {
            return none;
        }

        int r = rd.sdt.peekrbuf(0);
        if ((r & (0x4)) == 0x4) {
            return ttEmpty;
        } else if ((r & (0x1)) == 0x0) {
            return ttDone;
        } else {
            return none;
        }
    }

}
