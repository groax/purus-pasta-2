package haven.purus.gob.attribute;

import haven.GAttrib;
import haven.Gob;
import haven.Gob.Type;
import haven.ResDrawable;
import haven.render.MixColor;
import haven.render.Pipe;

public class GardenPotHighlight extends GAttrib implements Gob.SetupMod {
    private static final MixColor refillNeeded = new MixColor(255, 0, 0, 64);
    private static final MixColor done = new MixColor(0, 255, 0, 64);
    private static final MixColor none = new MixColor(0, 0, 0, 0);

    public GardenPotHighlight(Gob g) {
        super(g);
    }

    public static void processGob(Gob gob) {
        if (gob == null || gob.type != Type.GARDENPOT || gob.getattr(GardenPotHighlight.class) != null) {
            return;
        }

        GardenPotHighlight highlightAttribute = new GardenPotHighlight(gob);
        gob.setattr(highlightAttribute);
    }

    @Override
    public Pipe.Op placestate() {
        if (gob.ols.size() == 2) {
            return done;
        }

        ResDrawable rd = gob.getattr(ResDrawable.class);
        if (rd == null) {
            return none;
        }

        int r = rd.sdt.peekrbuf(0);
        if (r != 0x3) {
            return refillNeeded;
        }

        return none;
    }

}
