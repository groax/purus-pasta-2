package haven.purus.gob.attribute;

import java.awt.*;

import haven.*;
import haven.render.Homo3D;
import haven.render.Pipe;
import haven.render.RenderTree;

public class Quality extends GAttrib implements RenderTree.Node, PView.Render2D {

    private final Tex qualityText;

    public Quality(Gob gob, int qualityValue) {
        super(gob);
        this.qualityText = Text.std.renderstroked(String.format("q%s", qualityValue), new Color(255, 255, 255, 200),
                                                  Color.black).tex();
    }

    @Override
    public void draw(GOut g, Pipe state) {
        Coord sc = Homo3D.obj2view(new Coord3f(0, 0, 5), state, Area.sized(g.sz())).round2();
        if (sc.isect(Coord.z, g.sz())) {
            g.aimage(qualityText, sc, 0.5, 2.0);
        }
    }

}
