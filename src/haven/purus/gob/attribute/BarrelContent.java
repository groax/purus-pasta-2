package haven.purus.gob.attribute;

import java.awt.*;
import java.util.Optional;

import haven.*;
import haven.Gob.Overlay;
import haven.purus.Config;
import haven.render.Homo3D;
import haven.render.Pipe;
import haven.render.RenderTree;

public class BarrelContent extends GAttrib implements RenderTree.Node, PView.Render2D {

    private String lastRecordedName;
    private Tex content;

    public BarrelContent(final Gob g) {
        super(g);
    }

    public static void processGob(Gob gob) {
        if (gob.resourceName() == null || !gob.resourceName().startsWith("gfx/terobjs/barrel") || gob.getattr(BarrelContent.class) != null) {
            return;
        }

        BarrelContent barrelContent = new BarrelContent(gob);
        gob.setattr(barrelContent);
    }

    @Override
    public void draw(GOut g, Pipe state) {
        if (Config.annotateBarrel.val && content != null) {
            Coord sc = Homo3D.obj2view(new Coord3f(0, 0, 15), state, Area.sized(g.sz())).round2();
            if (sc.isect(Coord.z, g.sz())) {
                g.aimage(content, sc, 0.5, 2.0);
            }
        }
    }

    @Override
    public void ctick(double dt) {
        if (!Config.annotateBarrel.val) {
            return;
        }
        Optional<String> foundContentName = gob.ols.stream()
                                      .map(Overlay::name)
                                      .filter(name1 -> name1.startsWith("gfx/terobjs/barrel-"))
                                      .map(name1 -> name1.substring(name1.lastIndexOf("-") + 1))
                                      .findAny();
        if (foundContentName.isEmpty()) {
            lastRecordedName = null;
            content = null;
            return;
        }

        String contentName = foundContentName.get();
        if (contentName.equals(lastRecordedName)) {
            return;
        }

        lastRecordedName = contentName;
        content = Text.std.renderstroked(contentName, new Color(255, 255, 255, 200), Color.black).tex();
    }

}
