package haven.purus.gob;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class GobConfiguration {

    public final static Set<String> alarmitems = new HashSet<>(
            Arrays.asList("gfx/terobjs/herbs/flotsam", "gfx/terobjs/herbs/chimingbluebell", "gfx/terobjs/herbs/edelweiss", "gfx/terobjs/herbs/bloatedbolete",
                          "gfx/terobjs/herbs/glimmermoss", "gfx/terobjs/herbs/camomile", "gfx/terobjs/herbs/clay-cave", "gfx/terobjs/herbs/mandrake",
                          "gfx/terobjs/herbs/seashell"));

    public final static Set<String> locres = new HashSet<>(
            Arrays.asList("gfx/terobjs/saltbasin", "gfx/terobjs/abyssalchasm", "gfx/terobjs/windthrow", "gfx/terobjs/icespire", "gfx/terobjs/woodheart", "gfx/terobjs/jotunmussel",
                          "gfx/terobjs/guanopile", "gfx/terobjs/geyser", "gfx/terobjs/claypit", "gfx/terobjs/caveorgan", "gfx/terobjs/crystalpatch", "gfx/terobjs/fairystone",
                          "gfx/terobjs/lilypadlotus"));

}
