package haven.purus.foodhistory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class FoodEntryDto implements Serializable {

    private final String name;
    private final Map<String, Double> foodPoints;
    private final int count;

    public FoodEntryDto(String name) {
        this(name, new HashMap<>(), 0);
    }

    public FoodEntryDto(String name, Map<String, Double> foodPoints, int count) {
        this.name = name;
        this.foodPoints = foodPoints;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public FoodEntryDto withEatenFood(EatenFoodDto eatenFoodDto) {
        if (eatenFoodDto == null || !Objects.equals(name, eatenFoodDto.getName())) {
            return this;
        }

        int newCount = count + 1;
        Map<String, Double> newFoodPoints = mergeFoodEventPoints(eatenFoodDto);

        return new FoodEntryDto(name, newFoodPoints, newCount);
    }

    private Map<String, Double> mergeFoodEventPoints(EatenFoodDto eatenFoodDto) {
        Map<String, Double> newFoodPoints = new HashMap<>(foodPoints);
        Map<String, Double> foodEventPoints = eatenFoodDto.getFoodEventPoints();

        foodEventPoints.forEach((name, points) -> {
            Double currentValue = foodPoints.getOrDefault(name, 0.d);
            newFoodPoints.put(name, currentValue + points);
        });

        return newFoodPoints;
    }

}
