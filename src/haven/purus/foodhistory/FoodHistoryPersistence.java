package haven.purus.foodhistory;

import java.util.Optional;

import haven.purus.botdata.FoodHistoryData;

public class FoodHistoryPersistence {

    public Optional<FoodHistoryState> loadFoodHistory(String playerName) {
        return FoodHistoryData.loadState(playerName);
    }

    public long saveState(FoodHistoryState state) {
        long stateId = FoodHistoryData.upsertState(state.getPlayerName(), state.getRequiredFep(), state.getCurrentFep());
        state.getFoodEntries().values().forEach(entry -> addEntry(stateId, entry));

        return stateId;
    }

    public void addEntry(long foodHistoryId, FoodEntryDto foodEntryDto) {
        FoodHistoryData.upsertFoodEntry(foodHistoryId, foodEntryDto.getName(), foodEntryDto.getCount());
    }

    public long resetState(long previousStateId, FoodHistoryState newState) {
        FoodHistoryData.removeFoodEntries(previousStateId);
        return saveState(newState);
    }

}
