package haven.purus.foodhistory;

import java.awt.image.BufferedImage;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import haven.*;
import haven.WItem.ItemTip;
import haven.resutil.FoodInfo;

public class FoodHistoryServiceImpl implements FoodHistoryService {

    private final FoodHistoryPersistence foodHistoryPersistence;
    private final TooltipInfoToFoodConverter tooltipInfoToFoodConverter;
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    private FoodHistoryState foodHistoryState;

    private final GameUI gameUI;
    private boolean isInitialized = false;

    public FoodHistoryServiceImpl(GameUI gameUI) {
        this.gameUI = gameUI;
        this.foodHistoryPersistence = new FoodHistoryPersistence();
        this.tooltipInfoToFoodConverter = new TooltipInfoToFoodConverter();
        this.foodHistoryState = foodHistoryPersistence.loadFoodHistory(getPlayerName())
                                                      .filter(this::isValid)
                                                      .orElseGet(this::createEmptyState);
    }

    @Override
    public void addEatenFood(EatenFoodDto eatenFoodDto) {
        String name = eatenFoodDto.getName();
        Map<String, FoodEntryDto> foodEntries = foodHistoryState.getFoodEntries();

        FoodEntryDto entry = foodEntries.computeIfAbsent(name, FoodEntryDto::new);
        executor.submit(() -> foodHistoryPersistence.addEntry(foodHistoryState.getId(), entry));
        var updatedEntry = entry.withEatenFood(eatenFoodDto);
        foodEntries.put(name, updatedEntry);
    }

    @Override
    public void updateFepPoints(double requiredPoints, double currentPoints) {
        foodHistoryState.setRequiredFep(requiredPoints);
        foodHistoryState.setCurrentFep(currentPoints);
        persistFoodHistory();
    }

    @Override
    public void validateCurrentState(double requiredPoints, double currentPoints) {
        if (isInitialized || isCurrentStateUpToDate(requiredPoints, currentPoints)) {
            isInitialized = true;
            return;
        }

        reset(requiredPoints, currentPoints);
        isInitialized = true;
    }

    @Override
    public List<BufferedImage> getHistoryAsTextImages() {
        Map<String, FoodEntryDto> foodEntries = foodHistoryState.getFoodEntries();
        if (foodEntries.isEmpty()) {
            return new ArrayList<>();
        }

        return foodEntries.entrySet().stream().map(this::convertFoodEntryToTextImage).collect(Collectors.toList());
    }

    @Override
    public Set<String> getFoodNamesInHistory() {
        return foodHistoryState.getFoodEntries().keySet();
    }

    @Override
    public void persistFoodHistory() {
        CompletableFuture.supplyAsync(() -> foodHistoryPersistence.saveState(foodHistoryState), executor)
                         .thenAccept(foodHistoryState::setId);
    }

    @Override
    public void reset(double requiredPoints, double currentPoints) {
        long previousStateId = foodHistoryState.getId();
        foodHistoryState = createEmptyState();
        foodHistoryState.setRequiredFep(requiredPoints);
        foodHistoryState.setCurrentFep(currentPoints);

        CompletableFuture.supplyAsync(() -> foodHistoryPersistence.resetState(previousStateId, foodHistoryState),
                                      executor).thenAccept(foodHistoryState::setId);
        executor.submit(this::updateFoodItems);
    }

    @Override
    public void extractFoodInfoFromTooltip(ItemTip itemTip) {
        tooltipInfoToFoodConverter.extractEatenFood(itemTip).ifPresent(this::addEatenFood);
        executor.submit(this::updateFoodItems);
    }

    private boolean isCurrentStateUpToDate(double requiredPoints, double currentPoints) {
        double epsilon = 0.01;
        return Math.abs(foodHistoryState.getCurrentFep() - currentPoints) < epsilon &&
               Math.abs(foodHistoryState.getRequiredFep() - requiredPoints) < epsilon;
    }

    private boolean isValid(FoodHistoryState historyState) {
        String playerName = getPlayerName();
        return Objects.equals(playerName, historyState.getPlayerName());
    }

    private FoodHistoryState createEmptyState() {
        return new FoodHistoryState(getPlayerName(), 0, 0);
    }

    private String getPlayerName() {
        return gameUI.chrid;
    }

    private BufferedImage convertFoodEntryToTextImage(Map.Entry<String, FoodEntryDto> entry) {
        return Text.render(convertFoodEntryToString(entry)).img;
    }

    private String convertFoodEntryToString(Map.Entry<String, FoodEntryDto> entry) {
        FoodEntryDto foodEntryDto = entry.getValue();
        return foodEntryDto.getName() + " x" + foodEntryDto.getCount();
    }

    private void updateFoodItems() {
        if (gameUI == null) {
            return;
        }

        try {
            Set<GItem> children = gameUI.children(GItem.class);
            children.stream().filter(child -> {
                try {
                    return ItemInfo.find(FoodInfo.class, child.info()) != null;
                } catch (Loading ignored) {
                }
                return false;
            }).forEach(GItem::updateItemInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
