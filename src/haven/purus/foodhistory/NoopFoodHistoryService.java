package haven.purus.foodhistory;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import haven.WItem.ItemTip;

public class NoopFoodHistoryService implements FoodHistoryService {

    @Override
    public void addEatenFood(EatenFoodDto eatenFoodDto) {

    }

    @Override
    public void updateFepPoints(double requiredPoints, double currentPoints) {

    }

    @Override
    public void validateCurrentState(double requiredPoints, double currentPoints) {

    }

    @Override
    public List<BufferedImage> getHistoryAsTextImages() {
        return new ArrayList<>();
    }

    @Override
    public Set<String> getFoodNamesInHistory() {
        return new HashSet<>();
    }

    @Override
    public void persistFoodHistory() {

    }

    @Override
    public void reset(double requiredPoints, double currentPoints) {

    }

    @Override
    public void extractFoodInfoFromTooltip(ItemTip itemTip) {

    }

}
