package haven.purus.foodhistory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class FoodHistoryState implements Serializable {

    private long id;
    private final String playerName;
    private final Map<String, FoodEntryDto> foodEntries;
    private double requiredFep;
    private double currentFep;

    public FoodHistoryState(String playerName, double requiredFep, double currentFep) {
        this(-1, playerName, new HashMap<>(), requiredFep, currentFep);
    }

    public FoodHistoryState(long id, String playerName, Map<String, FoodEntryDto> foodEntries, double requiredFep, double currentFep) {
        this.id = id;
        this.playerName = playerName;
        this.foodEntries = foodEntries;
        this.requiredFep = requiredFep;
        this.currentFep = currentFep;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public Map<String, FoodEntryDto> getFoodEntries() {
        return foodEntries;
    }

    public double getRequiredFep() {
        return requiredFep;
    }

    public double getCurrentFep() {
        return currentFep;
    }

    public void setRequiredFep(double requiredFep) {
        this.requiredFep = requiredFep;
    }

    public void setCurrentFep(double currentFep) {
        this.currentFep = currentFep;
    }

}
