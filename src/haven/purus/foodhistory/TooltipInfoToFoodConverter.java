package haven.purus.foodhistory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import haven.ItemInfo.Name;
import haven.WItem.ItemTip;
import haven.Warning;
import haven.resutil.FoodInfo;
import haven.resutil.FoodInfo.Event;

public class TooltipInfoToFoodConverter {

    public Optional<EatenFoodDto> extractEatenFood(ItemTip itemTip) {
        if (itemTip == null || itemTip.item() == null) {
            new Warning("Tip is not set", getClassName()).issue();
            return Optional.empty();
        }

        Optional<Name> foundInfo = findNameInfo(itemTip);
        Optional<FoodInfo> foundFoodInfo = findFoodInfo(itemTip);
        if (!foundInfo.isPresent() || !foundFoodInfo.isPresent()) {
            new Warning("Name or food info not found", getClassName()).issue();
            return Optional.empty();
        }

        Name name = foundInfo.get();
        FoodInfo foodInfo = foundFoodInfo.get();

        String nameText = name.str.text;
        Map<String, Double> eventPoints = convertToEventPoints(foodInfo);
        return Optional.of(new EatenFoodDto(nameText, eventPoints));
    }

    private Map<String, Double> convertToEventPoints(FoodInfo foodInfo) {
        Event[] events = foodInfo.evs;
        if (events == null) {
            return new HashMap<>();
        }

        return Arrays.stream(events).collect(Collectors.toMap(event -> event.ev.nm, event -> event.a, Double::sum));
    }

    private Optional<FoodInfo> findFoodInfo(ItemTip itemTip) {
        return itemTip.item().info().stream().filter(itemInfo -> itemInfo instanceof FoodInfo).map(itemInfo -> (FoodInfo) itemInfo).findAny();
    }

    private Optional<Name> findNameInfo(ItemTip itemTip) {
        return itemTip.item().info().stream().filter(itemInfo -> itemInfo instanceof Name).map(itemInfo -> (Name) itemInfo).findAny();
    }

    private String getClassName() {
        return this.getClass().getName();
    }

}
