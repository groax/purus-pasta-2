package haven.purus.foodhistory;

import java.util.Map;

public class EatenFoodDto {

    private final String name;
    private final Map<String, Double> foodEventPoints;

    public EatenFoodDto(String name, Map<String, Double> foodEventPoints) {
        this.name = name;
        this.foodEventPoints = foodEventPoints;
    }

    public String getName() {
        return name;
    }

    public Map<String, Double> getFoodEventPoints() {
        return foodEventPoints;
    }

}
