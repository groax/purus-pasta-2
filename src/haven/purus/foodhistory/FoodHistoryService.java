package haven.purus.foodhistory;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Set;

import haven.WItem.ItemTip;

public interface FoodHistoryService {

    void addEatenFood(EatenFoodDto eatenFoodDto);

    void updateFepPoints(double requiredPoints, double currentPoints);

    void validateCurrentState(double requiredPoints, double currentPoints);

    List<BufferedImage> getHistoryAsTextImages();

    Set<String> getFoodNamesInHistory();

    void persistFoodHistory();

    void reset(double requiredPoints, double currentPoints);

    void extractFoodInfoFromTooltip(ItemTip itemTip);

}
