package haven.purus.pbot.hnhbot.dto;

public record ScriptManifest(String name, String botClassName) {

}
