package haven.purus.pbot.hnhbot.dto;

public record HnhBotConfiguration(String name, String path, String scriptClassName) {

}
