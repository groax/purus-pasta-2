package haven.purus.pbot.api.callback;

public interface Callback<T> {
	void callback(T obj);
}
