package haven.purus.mapper;

import haven.Coord;

public class MapRef {

    public Coord gc;
    public long mapID;

    MapRef(long mapID, Coord gc) {
        this.gc = gc;
        this.mapID = mapID;
    }

    public String toString() {
        return (gc.toString() + " in map space " + mapID);
    }

}
