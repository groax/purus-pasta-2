package haven.purus.mapper;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import haven.Button;
import haven.Label;
import haven.*;
import haven.purus.Config;
import haven.purus.Entry;

public class MapperConfiguration {

    private Entry mapServerUrl;
    private Entry mapTokenStatus;
    private Entry mapToken;

    public Entry createConfigurationUI() {
        Entry mapSettings = new Entry(new Label("Map Settings"), "Map Settings");
        ((Label) mapSettings.getW()).setcolor(Color.ORANGE);

        CheckBox addNewCustomMarkers = new CheckBox("Add new custom map markers");
        addNewCustomMarkers.a = Config.addNewCustomMarkers.val;
        addNewCustomMarkers.changed(Config.addNewCustomMarkers::setVal);
        mapSettings.addSubentry(new Entry(addNewCustomMarkers, "Add new custom map markers"));

        CheckBox displayCustomMarkers = new CheckBox("Display custom map markers");
        displayCustomMarkers.a = Config.displayCustomMarkers.val;
        displayCustomMarkers.changed(Config.displayCustomMarkers::setVal);
        mapSettings.addSubentry(new Entry(displayCustomMarkers, "Display custom map markers"));

        var mapDescriptionText = new RichTextBox(UI.scale(500, 50),
                                                 "With the token you can save markers and access other map features. " +
                                                 "If you want to share your markers and tokens in the map with your friends, copy and save your friends token here.");
        Entry mapExplanation = new Entry(mapDescriptionText, "map");
        mapSettings.addSubentry(mapExplanation);

        CheckBox enabledCheckbox = new CheckBox("Enable mapping");
        enabledCheckbox.a = Config.mapperEnabled.val;
        enabledCheckbox.changed(this::onMappingEnabledClicked);
        mapSettings.addSubentry(new Entry(enabledCheckbox, "Enable mapping"));

        CheckBox trackingCheckbox = new CheckBox("Enable tracking");
        trackingCheckbox.a = Config.mapperTracking.val;
        trackingCheckbox.changed(this::onMappingTrackerClicked);
        mapSettings.addSubentry(new Entry(trackingCheckbox, "Enable mapping"));

        CheckBox playerMarkerCheckbox = new CheckBox("Enable player markers upload (on start)");
        playerMarkerCheckbox.a = Config.mapperPlayerMarkers.val;
        playerMarkerCheckbox.changed(Config.mapperPlayerMarkers::setVal);
        mapSettings.addSubentry(new Entry(playerMarkerCheckbox, "Enable player markers upload (on start)"));

        Entry mapUrlLabel = new Entry(new Label("Map Url:"), "map server url");
        mapSettings.addSubentry(mapUrlLabel);

        mapServerUrl = new Entry(new TextEntry(UI.scale(500), Config.mapperUrl.val), "map server url");
        mapSettings.addSubentry(mapServerUrl);

        mapTokenStatus = new Entry(new Label("Map Token:"), "map token");
        mapSettings.addSubentry(mapTokenStatus);

        mapToken = new Entry(new TextEntry(UI.scale(500), Config.mapperToken.val), "map token");
        mapSettings.addSubentry(mapToken);

        var tokenWidget = initializeTokenEntry();
        Entry mapButtons = new Entry(tokenWidget, "maptoken save");
        mapSettings.addSubentry(mapButtons);

        return mapSettings;
    }

    public static void initAutomapper(UI ui, String playerName) {
        MappingClient.remove(playerName);

        MappingClient.init(ui.sess.glob, playerName);
        MappingClient automapper = MappingClient.getInstance(playerName).orElseThrow();
        automapper.SetEndpoint(Config.mapperUrl.val + Config.mapperToken.val);
        automapper.EnableGridUploads(Config.mapperEnabled.val);
        automapper.EnableTracking(Config.mapperTracking.val);
    }

    private void onMappingEnabledClicked(boolean status) {
        Config.mapperEnabled.setVal(status);
        var mappingClients = MappingClient.getAllInstances();
        mappingClients.forEach(client -> client.EnableGridUploads(Config.mapperEnabled.val));
    }

    private void onMappingTrackerClicked(boolean status) {
        Config.mapperTracking.setVal(status);
        var mappingClients = MappingClient.getAllInstances();
        mappingClients.forEach(client -> client.EnableTracking(Config.mapperTracking.val));
    }

    private Widget initializeTokenEntry() {
        var tokenWidget = new Widget(UI.scale(600, 50)) {
            @Override
            public void draw(GOut g) {
                for (Widget wdg = child; wdg != null; wdg = next) {
                    next = wdg.next;
                    wdg.visible = true;
                }
                super.draw(g);
            }
        };
        Button saveButton = new Button(UI.scale(120), "Save map settings", this::onSaveMapClick);
        tokenWidget.add(saveButton, UI.scale(0, 0));

        Button openButton = new Button(UI.scale(120), "Open", this::onOpenClick);
        tokenWidget.add(openButton, UI.scale(140, 0));
        return tokenWidget;
    }

    private void onSaveMapClick() {
        String newUrl = ((TextEntry) mapServerUrl.getW()).buf.line();
        String newToken = ((TextEntry) mapToken.getW()).buf.line();
        setUrl(newUrl);
        Config.mapperToken.setVal(newToken);
        var mappingClients = MappingClient.getAllInstances();
        mappingClients.forEach(client -> client.SetEndpoint(Config.mapperUrl.val + Config.mapperToken.val));
    }

    private void setUrl(String newUrl) {
        if (newUrl.endsWith("/")) {
            newUrl = newUrl.substring(0, newUrl.length() - 1);
        }
        Config.mapperUrl.setVal(newUrl);
    }

    private void onOpenClick() {
        try {
            Toolkit.getDefaultToolkit().beep();
            Optional<String> url = getUrlString();
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE) &&
                url.isPresent()) {
                Desktop.getDesktop().browse(new URI(url.get() + "/map"));
            }
        } catch (URISyntaxException | IOException e) {
        }
    }

    public static boolean isMappingEnabled() {
        return Config.mapperEnabled.val && getUrlString().isPresent() && getTokenString().isPresent();
    }

    public static Optional<String> getUrlString() {
        String urlValue = Config.mapperUrl.val;
        if (urlValue != null && !urlValue.isBlank()) {
            return Optional.of(urlValue);
        }

        return Optional.empty();
    }

    public static Optional<String> getTokenString() {
        String tokenValue = Config.mapperToken.val;
        if (tokenValue != null && !tokenValue.isBlank()) {
            return Optional.of(tokenValue);
        }

        return Optional.empty();
    }

    public static Optional<String> getUrlTokenString() {
        return getUrlString().flatMap(url -> getTokenString().map(token -> url + token));
    }

}
