package haven.purus.mapper;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.json.JSONArray;

class MarkerUpdate implements Runnable {

    private final MappingClient mappingClient;
    JSONArray data;

    MarkerUpdate(MappingClient mappingClient, JSONArray data) {
        this.mappingClient = mappingClient;
        this.data = data;
    }

    @Override
    public void run() {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(
                    mappingClient.getEndpoint() + "/markerUpdate").openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            connection.setDoOutput(true);
            try (DataOutputStream out = new DataOutputStream(connection.getOutputStream())) {
                final String json = data.toString();
                out.write(json.getBytes(StandardCharsets.UTF_8));
            }
            int code = connection.getResponseCode();
            if (code != 200) {
                System.out.printf("Failed to update markers with response code %s%n", code);
            }
            connection.disconnect();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

}
