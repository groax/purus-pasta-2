package haven.purus.mapper;

import haven.Indir;
import haven.MapFile;
import haven.MapFile.Grid;
import haven.MapFile.Marker;

class MarkerData {

    Marker m;
    Indir<Grid> indirGrid;

    MarkerData(Marker m, Indir<Grid> indirGrid) {
        this.m = m;
        this.indirGrid = indirGrid;
    }

}
