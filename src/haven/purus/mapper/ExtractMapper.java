package haven.purus.mapper;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import haven.Coord;
import haven.Indir;
import haven.MapFile;
import haven.MapFile.Grid;
import haven.MapFile.Marker;

class ExtractMapper implements Runnable {

    private final MappingClient mappingClient;
    MapFile mapfile;
    Predicate<Marker> uploadCheck;
    int retries = 5;

    ExtractMapper(MappingClient mappingClient, MapFile mapfile, Predicate<Marker> uploadCheck) {
        this.mappingClient = mappingClient;
        this.mapfile = mapfile;
        this.uploadCheck = uploadCheck;
    }

    @Override
    public void run() {
        if (mapfile.lock.readLock().tryLock()) {
            List<MarkerData> markers = mapfile.markers.stream().filter(uploadCheck).map(m -> {
                Coord mgc = new Coord(Math.floorDiv(m.tc.x, 100), Math.floorDiv(m.tc.y, 100));
                Indir<Grid> indirGrid = mapfile.segments.get(m.seg).grid(mgc);
                return new MarkerData(m, indirGrid);
            }).collect(Collectors.toList());
            System.out.println("collected " + markers.size() + " markers");
            mapfile.lock.readLock().unlock();
            mappingClient.scheduler.execute(new ProcessMapper(mappingClient, mapfile, markers));
        } else {
            if (retries-- > 0) {
                System.out.println("rescheduling upload");
                mappingClient.scheduler.schedule(this, 5, TimeUnit.SECONDS);
            }
        }
    }

}
