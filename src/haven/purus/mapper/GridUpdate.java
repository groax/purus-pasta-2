package haven.purus.mapper;

import java.lang.ref.WeakReference;
import java.util.Map;

import haven.MCache.Grid;

class GridUpdate {

    String[][] grids;
    Map<String, WeakReference<Grid>> gridRefs;

    GridUpdate(final String[][] grids, Map<String, WeakReference<Grid>> gridRefs) {
        this.grids = grids;
        this.gridRefs = gridRefs;
    }

    @Override
    public String toString() {
        return String.format("GridUpdate (%s)", grids[1][1]);
    }

}
