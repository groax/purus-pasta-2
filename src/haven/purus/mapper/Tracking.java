package haven.purus.mapper;

import haven.Coord2d;
import org.json.JSONObject;

class Tracking {

    public String name;
    public String type;
    public long gridId;
    public Coord2d coords;

    public JSONObject getJSON() {
        JSONObject j = new JSONObject();
        j.put("name", name);
        j.put("type", type);
        j.put("gridID", String.valueOf(gridId));
        JSONObject c = new JSONObject();
        c.put("x", (int) (coords.x / 11));
        c.put("y", (int) (coords.y / 11));
        j.put("coords", c);
        return j;
    }

}
