package haven.purus.mapper;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import haven.*;
import org.json.JSONObject;

class PositionUpdates implements Runnable {

    private final MappingClient mappingClient;

    private final Map<Long, Tracking> tracking = new ConcurrentHashMap<Long, Tracking>();

    private int spamPreventionVal = 3;
    private int spamCount = 0;

    PositionUpdates(MappingClient mappingClient) {
        this.mappingClient = mappingClient;
    }

    void Track(long id, Coord2d coordinates, long gridId) {
        Tracking t = tracking.get(id);
        if (t == null) {
            t = new Tracking();
            tracking.put(id, t);

            if (id == mappingClient.getGlob().sess.ui.gui.map.plgob) {
                t.name = mappingClient.getPlayerName();
                t.type = "player";
            } else {
                Glob g = mappingClient.getGlob();
                Gob gob = g.oc.getgob(id);
                t.name = "???";
                t.type = "white";
                if (gob != null) {
                    KinInfo ki = gob.getattr(KinInfo.class);
                    if (ki != null) {
                        t.name = ki.name;
                        t.type = Integer.toHexString(BuddyWnd.gc[ki.group].getRGB());
                    }
                }
            }
        }
        t.gridId = gridId;
        t.coords = MappingClient.gridOffset(coordinates);
    }

    @Override
    public void run() {
        if (spamCount == spamPreventionVal) {
            spamCount = 0;
            if (mappingClient.isTrackingEnabled()) {
                Glob g = mappingClient.getGlob();
                Iterator<Entry<Long, Tracking>> i = tracking.entrySet().iterator();
                JSONObject upload = new JSONObject();
                while (i.hasNext()) {
                    Entry<Long, Tracking> e = i.next();
                    if (g.oc.getgob(e.getKey()) == null) {
                        i.remove();
                    } else {
                        upload.put(String.valueOf(e.getKey()), e.getValue().getJSON());
                    }
                }

                try {
                    final HttpURLConnection connection = (HttpURLConnection) new URL(
                            mappingClient.getEndpoint() + "/positionUpdate").openConnection();
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    connection.setDoOutput(true);
                    try (DataOutputStream out = new DataOutputStream(connection.getOutputStream())) {
                        final String json = upload.toString();
                        out.write(json.getBytes(StandardCharsets.UTF_8));
                    } catch (Exception e) {
                    }
                    int responseCode = connection.getResponseCode();
                    if (responseCode != 200) {
                        System.out.printf("Failed to update position with response code %s%n", responseCode);
                    }
                } catch (final Exception ex) {
                }
            }
        } else {
            spamCount++;
        }
    }

}
