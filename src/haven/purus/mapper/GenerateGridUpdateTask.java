package haven.purus.mapper;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import haven.Coord;
import haven.MCache.Grid;
import haven.MCache.LoadingMap;

class GenerateGridUpdateTask implements Runnable {

    private final MappingClient mappingClient;
    Coord coord;
    int retries = 3;

    GenerateGridUpdateTask(MappingClient mappingClient, Coord c) {
        this.mappingClient = mappingClient;
        this.coord = c;
    }

    @Override
    public void run() {
        if (mappingClient.isGridEnabled()) {
            final String[][] gridMap = new String[3][3];
            Map<String, WeakReference<Grid>> gridRefs = new HashMap<String, WeakReference<Grid>>();
            try {
                for (int x = -1; x <= 1; x++) {
                    for (int y = -1; y <= 1; y++) {
                        final Grid subg = mappingClient.getGlob().map.getgrid(coord.add(x, y));
                        gridMap[x + 1][y + 1] = String.valueOf(subg.id);
                        gridRefs.put(String.valueOf(subg.id), new WeakReference<Grid>(subg));
                    }
                }
                mappingClient.scheduler.execute(
                        new UploadGridUpdateTask(mappingClient, new GridUpdate(gridMap, gridRefs)));
            } catch (LoadingMap lm) {
                retries--;
                if (retries >= 0) {
                    mappingClient.scheduler.schedule(this, 1L, TimeUnit.SECONDS);
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

}
