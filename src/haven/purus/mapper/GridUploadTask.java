package haven.purus.mapper;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

import haven.Loading;
import haven.MCache.Grid;
import org.json.JSONObject;

class GridUploadTask implements Runnable {

    private final MappingClient mappingClient;
    private final String gridID;
    private final WeakReference<Grid> grid;

    GridUploadTask(MappingClient mappingClient, String gridID, WeakReference<Grid> grid) {
        this.mappingClient = mappingClient;
        this.gridID = gridID;
        this.grid = grid;
    }

    @Override
    public void run() {
        try {
            Grid g = grid.get();
            if (g != null && mappingClient.getGlob() != null && mappingClient.getGlob().map != null) {
                BufferedImage image = MinimapImageGenerator.drawmap(mappingClient.getGlob().map, g);
                if (image == null) {
                    throw new Loading();
                }
                try {
                    JSONObject extraData = new JSONObject();
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    ImageIO.write(image, "png", outputStream);
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
                    MultipartUtility multipart = new MultipartUtility(mappingClient.getEndpoint() + "/gridUpload", "utf-8");
                    multipart.addFormField("id", this.gridID);
                    multipart.addFilePart("file", inputStream, "minimap.png");
                    extraData.put("season", mappingClient.getGlob().ast.is);
                    multipart.addFormField("extraData", extraData.toString());
                    MultipartUtility.Response response = multipart.finish();
                    if (response.statusCode != 200) {
                        System.out.println("Upload Error: Code" + response.statusCode + " - " + response.response);
                    } else {

                    }
                } catch (IOException e) {
                    System.out.println("Cannot upload " + gridID + ": " + e.getMessage());
                }
            }
        } catch (Loading ex) {
            // Retry on Loading
            mappingClient.submitGridUpload(this);
        }

    }

}
