package haven.purus.mapper;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Predicate;

import haven.*;
import haven.MapFile.Marker;
import org.json.JSONArray;


/**
 * @author Vendan
 */
public class MappingClient {

    private static final Map<String, MappingClient> INSTANCES = new ConcurrentHashMap<>();
    private final ExecutorService gridsUploader = Executors.newSingleThreadExecutor();
    private final Glob glob;
    private final Map<Long, MapRef> cache = new HashMap<>();
    private final PositionUpdates pu = new PositionUpdates(this);
    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);
    private boolean trackingEnabled;
    private boolean gridEnabled;
    private String endpoint;
    private String playerName;
    private Coord lastGC = null;

    private MappingClient(Glob glob, String playerName) {
        this.glob = glob;
        this.playerName = playerName;
        scheduler.scheduleAtFixedRate(pu, 2L, 2L, TimeUnit.SECONDS);
    }

    public static void init(Glob glob, String playerName) {
        synchronized (MappingClient.class) {
            if (MappingClient.getInstance(playerName).isEmpty()) {
                INSTANCES.put(playerName, new MappingClient(glob, playerName));
            } else {
                throw new IllegalStateException("MappingClient can only be initialized once!");
            }
        }
    }

    public static void remove(String playerName) {
        synchronized (MappingClient.class) {
            var removed = INSTANCES.remove(playerName);
            if (removed != null) {
                removed.destroy();
            }
        }
    }

    public void destroy() {
        synchronized (MappingClient.class) {
            gridsUploader.shutdown();
            scheduler.shutdown();
        }
    }

    public static Optional<MappingClient> getInstance(Glob glob) {
        return Optional.ofNullable(glob)
                       .map(g -> g.sess)
                       .map(s -> s.ui)
                       .map(u -> u.gui)
                       .map(g -> g.chrid)
                       .flatMap(MappingClient::getInstance);
    }

    public static Optional<MappingClient> getInstance(String playerName) {
        synchronized (MappingClient.class) {
            return Optional.ofNullable(INSTANCES.get(playerName));
        }
    }

    public static Collection<MappingClient> getAllInstances() {
        synchronized (MappingClient.class) {
            return INSTANCES.values();
        }
    }

    private static Coord toGC(Coord2d c) {
        return new Coord(Math.floorDiv((int) c.x, 1100), Math.floorDiv((int) c.y, 1100));
    }

    private static Coord toGridUnit(Coord2d c) {
        return new Coord(Math.floorDiv((int) c.x, 1100) * 1100, Math.floorDiv((int) c.y, 1100) * 1100);
    }

    static Coord2d gridOffset(Coord2d c) {
        Coord gridUnit = toGridUnit(c);
        return new Coord2d(c.x - gridUnit.x, c.y - gridUnit.y);
    }

    public String getPlayerName() {
        return playerName;
    }

    public boolean isTrackingEnabled() {
        return trackingEnabled;
    }

    public Glob getGlob() {
        return glob;
    }

    public String getEndpoint() {
        return endpoint;
    }

    /***
     * Enable tracking for this execution.  Must be called each time the client is started.
     * @param enabled
     */
    public void EnableTracking(boolean enabled) {
        trackingEnabled = enabled;
    }

    /***
     * Enable grid data/image upload for this execution.  Must be called each time the client is started.
     * @param enabled
     */
    public void EnableGridUploads(boolean enabled) {
        gridEnabled = enabled;
    }

    /***
     * Set mapping server endpoint.  Must be called each time the client is started.  Takes effect immediately.
     * @param endpoint
     */
    public void SetEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    /***
     * Set the player name.  Typically called from Charlist.wdgmsg
     * @param name
     */
    public void SetPlayerName(String name) {
        playerName = name;
    }

    /***
     * Checks that the endpoint is functional and matches the version of this mapping client.
     * @return
     */
    public boolean CheckEndpoint() {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(
                    endpoint + "/checkVersion?version=4").openConnection();
            connection.setRequestMethod("GET");
            return connection.getResponseCode() == 200;
        } catch (Exception ex) {
            return false;
        }
    }

    /***
     * Track a gob at a location.  Typically called in Gob.move
     * @param id
     * @param coordinates
     */
    public void Track(long id, Coord2d coordinates) {
        try {
            MCache.Grid g = glob.map.getgrid(toGC(coordinates));
            pu.Track(id, coordinates, g.id);
        } catch (Exception ex) {
        }
    }

    /***
     * Called when entering a new grid
     * @param gc Grid coordinates
     */
    public void EnterGrid(Coord gc) {
        lastGC = gc;
        scheduler.execute(new GenerateGridUpdateTask(this, gc));
    }

    /***
     * Called as you move around, automatically calculates if you have entered a new grid and calls EnterGrid accordingly.
     * @param c Normal coordinates
     */
    public void CheckGridCoord(Coord2d c) {
        Coord gc = toGC(c);
        if (lastGC == null || !gc.equals(lastGC)) {
            EnterGrid(gc);
        }
    }

    /***
     * Gets a MapRef (mapid, coordinate pair) for the players current location
     * @return Current grid MapRef
     */
    public MapRef GetMapRef() {
        try {
            Gob player = glob.sess.ui.gui.map.player();
            Coord gc = toGC(player.rc);
            synchronized (cache) {
                long id = glob.map.getgrid(gc).id;
                MapRef mapRef = cache.get(id);
                if (mapRef == null) {
                    scheduler.execute(new Locate(this, id));
                }
                return mapRef;
            }
        } catch (Exception e) {
        }
        return null;
    }

    /***
     * Given a mapref, opens the map to the corresponding location
     * @param mapRef
     */
    public void OpenMap(MapRef mapRef) {
        try {
            if (mapRef == null) {
                return;
            }
            WebBrowser.self.show(
                    new URL(String.format(endpoint + "/#/grid/%d/%d/%d/6", mapRef.mapID, mapRef.gc.x, mapRef.gc.y)));
        } catch (Exception ex) {
        }
    }

    /***
     * Process a mapfile to extract markers to upload
     * @param mapfile
     * @param uploadCheck
     */
    public void ProcessMap(MapFile mapfile, Predicate<Marker> uploadCheck) {
        scheduler.schedule(new ExtractMapper(this, mapfile, uploadCheck), 5, TimeUnit.SECONDS);

    }

    public boolean isGridEnabled() {
        return gridEnabled;
    }

    void submitGridUpload(GridUploadTask gridUploadTask) {
        gridsUploader.submit(gridUploadTask);
    }

    public void cacheReference(long gridID, MapRef mr) {
        synchronized (cache) {
            cache.put(gridID, mr);
        }
    }

    public void scheduleMarkerUpdate(JSONArray objects) {
        try {
            scheduler.execute(new MarkerUpdate(this, objects));
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public void cacheGrid(Long gridId, MapRef mapRef) {
        synchronized (cache) {
            cache.put(gridId, mapRef);
        }
    }

    void scheduleGridUpload(GridUploadTask gridUploadTask) {
        gridsUploader.execute(gridUploadTask);
    }

}
