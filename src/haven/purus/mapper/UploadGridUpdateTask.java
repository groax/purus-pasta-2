package haven.purus.mapper;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import haven.Coord;
import org.json.JSONArray;
import org.json.JSONObject;

class UploadGridUpdateTask implements Runnable {

    private final MappingClient mappingClient;
    private final GridUpdate gridUpdate;

    UploadGridUpdateTask(MappingClient mappingClient, final GridUpdate gridUpdate) {
        this.mappingClient = mappingClient;
        this.gridUpdate = gridUpdate;
    }

    @Override
    public void run() {
        if (mappingClient.isGridEnabled()) {
            HashMap<String, Object> dataToSend = new HashMap<>();

            dataToSend.put("grids", this.gridUpdate.grids);
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(
                        mappingClient.getEndpoint() + "/gridUpdate").openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                connection.setDoOutput(true);
                try (DataOutputStream out = new DataOutputStream(connection.getOutputStream())) {
                    String json = new JSONObject(dataToSend).toString();
                    out.write(json.getBytes(StandardCharsets.UTF_8));
                }
                int responseCode = connection.getResponseCode();
                if (responseCode == 200) {
                    DataInputStream dio = new DataInputStream(connection.getInputStream());
                    int nRead;
                    byte[] data = new byte[1024];
                    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                    while ((nRead = dio.read(data, 0, data.length)) != -1) {
                        buffer.write(data, 0, nRead);
                    }
                    buffer.flush();
                    String response = buffer.toString(StandardCharsets.UTF_8.name());
                    JSONObject jo = new JSONObject(response);
                    JSONArray reqs = jo.optJSONArray("gridRequests");
                    MapRef mapRef = new MapRef(jo.getLong("map"), new Coord(jo.getJSONObject("coords").getInt("x"),
                                                                            jo.getJSONObject("coords").getInt("y")));
                    mappingClient.cacheGrid(Long.valueOf(gridUpdate.grids[1][1]), mapRef);

                    for (int i = 0; reqs != null && i < reqs.length(); i++) {
                        GridUploadTask gridUploadTask = new GridUploadTask(mappingClient, reqs.getString(i),
                                                                           gridUpdate.gridRefs.get(reqs.getString(i)));
                        mappingClient.scheduleGridUpload(gridUploadTask);
                    }
                } else {
                    System.out.printf("Failed to update map grids with response code %s%n", responseCode);
                }

            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }

}
