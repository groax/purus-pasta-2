package haven.purus.mapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.stream.Collectors;

import haven.Coord;

class Locate implements Runnable {

    private final MappingClient mappingClient;
    long gridID;

    Locate(MappingClient mappingClient, long gridID) {
        this.mappingClient = mappingClient;
        this.gridID = gridID;
    }

    @Override
    public void run() {
        try {
            final HttpURLConnection connection = (HttpURLConnection) new URL(
                    mappingClient.getEndpoint() + "/locate?gridID=" + gridID).openConnection();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                String resp = reader.lines().collect(Collectors.joining());
                String[] parts = resp.split(";");
                if (parts.length == 3) {
                    MapRef mr = new MapRef(Integer.valueOf(parts[0]),
                                           new Coord(Integer.valueOf(parts[1]), Integer.valueOf(parts[2])));
                    mappingClient.cacheReference(gridID, mr);
                }

            } finally {
                connection.disconnect();
            }

        } catch (final Exception ex) {
        }
    }

}
