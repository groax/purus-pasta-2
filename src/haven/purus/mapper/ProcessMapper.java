package haven.purus.mapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import haven.Coord;
import haven.Loading;
import haven.MapFile;
import haven.MapFile.Grid;
import haven.MapFile.PMarker;
import haven.MapFile.SMarker;
import org.json.JSONArray;
import org.json.JSONObject;

class ProcessMapper implements Runnable {

    private final MappingClient mappingClient;
    MapFile mapfile;
    List<MarkerData> markers;

    ProcessMapper(MappingClient mappingClient, MapFile mapfile, List<MarkerData> markers) {
        this.mappingClient = mappingClient;
        this.mapfile = mapfile;
        this.markers = markers;
    }

    @Override
    public void run() {
        ArrayList<JSONObject> loadedMarkers = new ArrayList<>();
        while (!markers.isEmpty()) {
            System.out.println("processing " + markers.size() + " markers");
            Iterator<MarkerData> iterator = markers.iterator();
            while (iterator.hasNext()) {
                MarkerData md = iterator.next();
                try {
                    Coord mgc = new Coord(Math.floorDiv(md.m.tc.x, 100), Math.floorDiv(md.m.tc.y, 100));
                    Grid grid = md.indirGrid.get();
                    if (grid == null) {
                        iterator.remove();
                        continue;
                    }
                    long gridId = grid.id;
                    JSONObject o = new JSONObject();
                    o.put("name", md.m.nm);
                    o.put("gridID", String.valueOf(gridId));
                    Coord gridOffset = md.m.tc.sub(mgc.mul(100));
                    o.put("x", gridOffset.x);
                    o.put("y", gridOffset.y);

                    if (md.m instanceof SMarker) {
                        o.put("type", "shared");
                        o.put("id", ((SMarker) md.m).oid);
                        o.put("image", ((SMarker) md.m).res.name);
                    } else if (md.m instanceof PMarker) {
                        o.put("type", "player");
                        o.put("color", ((PMarker) md.m).color);
                    }
                    loadedMarkers.add(o);
                    iterator.remove();
                } catch (Loading ex) {
                    System.out.println(ex);
                }
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
            }
        }
        System.out.println("scheduling marker upload");
        mappingClient.scheduleMarkerUpdate(new JSONArray(loadedMarkers.toArray()));
        try {
            mappingClient.scheduler.execute(new MarkerUpdate(mappingClient, new JSONArray(loadedMarkers.toArray())));
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

}
