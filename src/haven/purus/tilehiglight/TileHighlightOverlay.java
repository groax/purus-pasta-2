package haven.purus.tilehiglight;

import java.awt.*;

import haven.Coord;
import haven.MapFile;

import static haven.MCache.cmaps;

class TileHighlightOverlay {

    private final boolean[] ol;

    public TileHighlightOverlay(MapFile.DataGrid g) {
        this.ol = new boolean[cmaps.x * cmaps.y];
        fill(g);
    }

    private void fill(MapFile.DataGrid grid) {
        if (grid == null) {
            return;
        }
        Coord c = new Coord(0, 0);
        for (c.x = 0; c.x < cmaps.x; c.x++) {
            for (c.y = 0; c.y < cmaps.y; c.y++) {
                int tile = grid.gettile(c);
                MapFile.TileInfo tileset = grid.tilesets[tile];
                boolean v = TileHighlight.isHighlighted(tileset.res.name);
                set(c, v);
                if (v) {
                    setn(c, true);
                } //make 1 tile border around actual tiles
            }
        }
    }

    public boolean get(Coord c) {
        return (ol[c.x + (c.y * cmaps.x)]);
    }

    public void set(Coord c, boolean v) {
        ol[c.x + (c.y * cmaps.x)] = v;
    }

    public void set(int x, int y, boolean v) {
        if (x >= 0 && y >= 0 && x < cmaps.x && y < cmaps.y) {
            ol[x + (y * cmaps.x)] = v;
        }
    }

    public void setn(Coord c, boolean v) {
        set(c.x - 1, c.y - 1, v);
        set(c.x - 1, c.y + 1, v);
        set(c.x + 1, c.y - 1, v);
        set(c.x + 1, c.y + 1, v);
        set(c.x, c.y - 1, v);
        set(c.x, c.y + 1, v);
        set(c.x - 1, c.y, v);
        set(c.x + 1, c.y, v);
    }

    public Color color() {
        return Color.MAGENTA;
    }

}
