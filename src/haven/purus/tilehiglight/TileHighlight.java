package haven.purus.tilehiglight;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.List;
import java.util.*;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import haven.*;

import static haven.MCache.cmaps;

public class TileHighlight {

    private static final Set<String> highlight = new HashSet<>();
    private static final Map<String, List<TileItem>> tiles = new HashMap<>();
    private static final List<String> categories = new ArrayList<>();
    public static final String TAG = "tileHighlight";
    private static boolean initialized = false;
    public static final String ALL = "All";

    public static String lastCategory = ALL;
    public static volatile long seq = 0;

    public static final Text.Foundry elf = CharWnd.attrf;
    public static final Color every = new Color(255, 255, 255, 16);
    public static final Color other = new Color(255, 255, 255, 32);
    public static final int elh = elf.height() + UI.scale(2);

    public static boolean isHighlighted(String name) {
        synchronized (highlight) {
            return highlight.contains(name);
        }
    }

    public static void toggle(String name) {
        synchronized (highlight) {
            if (highlight.contains(name)) {
                unhighlight(name);
            } else {
                highlight(name);
            }
        }
    }

    public static void highlight(String name) {
        synchronized (highlight) {
            if (highlight.add(name)) {
                seq++;
            }
        }
    }

    public static void unhighlight(String name) {
        synchronized (highlight) {
            if (highlight.remove(name)) {
                seq++;
            }
        }
    }

    public static BufferedImage olrender(MapFile.DataGrid grid) {
        TileHighlightOverlay ol = new TileHighlightOverlay(grid);
        WritableRaster buf = PUtils.imgraster(cmaps);
        Color col = ol.color();
        if (col != null) {
            Coord c = new Coord();
            for (c.y = 0; c.y < cmaps.y; c.y++) {
                for (c.x = 0; c.x < cmaps.x; c.x++) {
                    if (ol.get(c)) {
                        buf.setSample(c.x, c.y, 0, ((col.getRed() * col.getAlpha()) +
                                                    (buf.getSample(c.x, c.y, 1) * (255 - col.getAlpha()))) / 255);
                        buf.setSample(c.x, c.y, 1, ((col.getGreen() * col.getAlpha()) +
                                                    (buf.getSample(c.x, c.y, 1) * (255 - col.getAlpha()))) / 255);
                        buf.setSample(c.x, c.y, 2, ((col.getBlue() * col.getAlpha()) +
                                                    (buf.getSample(c.x, c.y, 2) * (255 - col.getAlpha()))) / 255);
                        buf.setSample(c.x, c.y, 3, Math.max(buf.getSample(c.x, c.y, 3), col.getAlpha()));
                    }
                }
            }
        }
        return (PUtils.rasterimg(buf));
    }

    public static void toggle(UI ui) {
        tryInit();
        if (ui.gui.tileHighlight == null) {
            ui.gui.tileHighlight = ui.gui.add(new TileHighlightCFG(), ui.gui.mapfile.c);
        } else {
            ui.gui.tileHighlight.destroy();
        }
    }

    private static void tryInit() {
        if (initialized) {
            return;
        }
        initialized = true;
        categories.add(ALL);
        ArrayList<TileItem> all = new ArrayList<>();
        tiles.put(ALL, all);

        TypeReference<Map<String, List<String>>> typeRef = new TypeReference<>() {
        };
        ObjectMapper mapper = new ObjectMapper();
        Map<String, List<String>> c = new HashMap<>();
        try {
            c = mapper.readValue(Config.loadJarFile("tile_highlight.json"), typeRef);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Map.Entry<String, List<String>> entry : c.entrySet()) {
            String category = entry.getKey();
            categories.add(category);
            List<String> tiles = entry.getValue();
            List<TileItem> items = new ArrayList<>(tiles.size());
            for (String tile : tiles) {
                items.add(new TileItem(tile));
            }
            items.sort(Comparator.comparing(TileItem::getName));
            TileHighlight.tiles.put(category, items);
            all.addAll(items);
        }
        all.sort(Comparator.comparing(TileItem::getName));
    }

    static Map<String, List<TileItem>> getTiles() {
        return tiles;
    }

    static List<String> getCategories() {
        return categories;
    }

}
