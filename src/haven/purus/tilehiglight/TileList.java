package haven.purus.tilehiglight;

import java.util.ArrayList;
import java.util.List;

import haven.Coord;
import haven.SSearchBox;

class TileList extends SSearchBox<TileItem, TileItemLine> {

    private List<TileItem> tileItems = new ArrayList<>();

    public TileList(int w, int h) {
        super(new Coord(w, h), TileHighlight.elh);
    }

    @Override
    protected boolean searchmatch(TileItem item, String filter) {
        if (filter.isEmpty()) {
            return true;
        }
        if (item.getName() == null) {
            return false;
        }
        return item.getName().toLowerCase().contains(filter.toLowerCase());
    }

    @Override
    protected List<? extends TileItem> allitems() {
        return tileItems;
    }

    public boolean keydown(java.awt.event.KeyEvent ev) {
        if (ev.getKeyCode() == java.awt.event.KeyEvent.VK_SPACE) {
            if (sel != null) {
                TileHighlight.toggle(sel.getRes());
            }
            return true;
        }
        return super.keydown(ev);
    }

    public void setItems(List<TileItem> tileItems) {
        this.tileItems = tileItems;
    }

    @Override
    protected TileItemLine makeitem(TileItem item, int idx, Coord sz) {
        return new TileItemLine(this, sz, item);
    }

}
