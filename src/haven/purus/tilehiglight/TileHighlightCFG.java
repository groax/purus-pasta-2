package haven.purus.tilehiglight;

import haven.*;
import haven.Label;
import haven.Window;

public class TileHighlightCFG extends Window {

    public static final String FILTER_DEFAULT = "Start typing to filter";

    private boolean raised = false;
    private final TileList list;
    private final haven.Label filter;
    public String category = TileHighlight.ALL;

    public TileHighlightCFG() {
        super(Coord.z, "Tile Highlight");

        int h = add(new haven.Label("Categories: "), Coord.z).sz.y;
        var selectAllCheckbox = new CheckBox("Select All");
        selectAllCheckbox.changed(this::onSelectAllCheckbox);
        add(selectAllCheckbox, UI.scale(135, 0));
        h += UI.scale(5);

        add(new CategoryList(this, UI.scale(125), 8, TileHighlight.elh), 0, h).sel = category;

        list = add(new TileList(UI.scale(220), UI.scale(8 * TileHighlight.elh)), UI.scale(135), h);
        filter = adda(new Label(FILTER_DEFAULT), list.pos("ur").y(0), 1, 0);
        pack();
        setfocus(list);
        list.setItems(TileHighlight.getTiles().get(category));
    }

    public TileList getList() {
        return list;
    }

    private void onSelectAllCheckbox(boolean val) {
        list.items().forEach(item -> {
            if (val) {
                TileHighlight.highlight(item.getRes());
            } else {
                TileHighlight.unhighlight(item.getRes());
            }
        });
    }

    @Override
    public void wdgmsg(Widget sender, String msg, Object... args) {
        if (sender == this) {
            reqdestroy();
        } else {
            super.wdgmsg(sender, msg, args);
        }
    }

    @Override
    public void tick(double dt) {
        super.tick(dt);
        if (!raised) {
            parent.setfocus(this);
            raise();
            raised = true;
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        ui.gui.tileHighlight = null;
    }

}
