package haven.purus.tilehiglight;

import haven.Tex;

class TileItem {

    private final String name, res;
    private final Tex tex;

    TileItem(String res) {
        this.res = res;
        this.name = TileNames.prettyResName(res);
        this.tex = TileHighlight.elf.render(this.name).tex();
    }

    public String getName() {
        return name;
    }

    public String getRes() {
        return res;
    }

}
