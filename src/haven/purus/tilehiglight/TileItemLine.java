package haven.purus.tilehiglight;

import haven.*;

class TileItemLine extends SListWidget.ItemWidget<TileItem> {

    public TileItemLine(TileList list, Coord sz, TileItem item) {
        super(list, sz, item);
        Widget checkbox = new CheckBox(item.getName()).state(() -> TileHighlight.isHighlighted(item.getRes()))
                                                       .set(this::onChange)
                                                       .settip("Highlight");
        add(checkbox, Coord.z);
    }

    private void onChange(boolean state) {
        TileHighlight.toggle(item.getRes());
    }

}
