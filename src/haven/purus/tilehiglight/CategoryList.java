package haven.purus.tilehiglight;

import java.util.Collections;

import haven.Coord;
import haven.GOut;
import haven.Listbox;

class CategoryList extends Listbox<String> {

    private final TileHighlightCFG tileHighlightCFG;

    public CategoryList(TileHighlightCFG tileHighlightCFG, int w, int h, int itemh) {
        super(w, h, itemh);
        this.tileHighlightCFG = tileHighlightCFG;
    }

    @Override
    public void change(String item) {
        super.change(item);
        tileHighlightCFG.getList().setItems(TileHighlight.getTiles().getOrDefault(item, Collections.emptyList()));
        tileHighlightCFG.getList().sb.val = 0;
        tileHighlightCFG.category = TileHighlight.lastCategory = item;
    }

    @Override
    protected String listitem(int i) {
        return TileHighlight.getCategories().get(i);
    }

    @Override
    protected int listitems() {
        return TileHighlight.getCategories().size();
    }

    @Override
    protected void drawitem(GOut g, String item, int i) {
        g.chcolor(((i % 2) == 0) ? TileHighlight.every : TileHighlight.other);
        g.frect(Coord.z, g.sz());
        g.chcolor();
        g.atext(item, new Coord(0, TileHighlight.elh / 2), 0, 0.5);
    }

}
