package haven.purus.tilehiglight;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import haven.Config;

class TileNames {

    private static final Pattern RESID = Pattern.compile(".*\\[([^,]*),?.*]");
    private static final Map<String, String> customNames = new HashMap<>();
    private static boolean customNamesInit = false;

    public static String prettyResName(String resname) {
        tryInitCustomNames();
        if(customNames.containsKey(resname)) {
            return customNames.get(resname);
        }
        Matcher m = RESID.matcher(resname);
        if(m.matches()) {
            resname = m.group(1);
        }
        int k = resname.lastIndexOf("/");
        resname = resname.substring(k + 1);
        resname = resname.substring(0, 1).toUpperCase() + resname.substring(1);
        return resname;
    }

    private static void tryInitCustomNames() {
        if(customNamesInit) {return;}
        customNamesInit = true;
        try {
            TypeReference<Map<String, String>> typeRef = new TypeReference<>() {
            };
            ObjectMapper mapper = new ObjectMapper();
            customNames.putAll(mapper.readValue(Config.loadJarFile("tile_names.json"), typeRef));
        } catch (Exception ignored) {
        }
    }

}
