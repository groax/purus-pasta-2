package haven.purus;

import java.util.ArrayList;
import java.util.List;

import haven.Widget;

public class Entry {

    String keywords;
    Widget w;

    private List<Entry> subentries = new ArrayList<>();

    public Entry(Widget w, String keywords) {
        this.keywords = keywords;
        this.w = w;
    }

    public void addSubentry(Entry e) {
        subentries.add(e);
    }

    List<Widget> match(String keyword) {
        ArrayList<Widget> l = new ArrayList<>();
        if (this.keywords.toLowerCase().contains(keyword.toLowerCase())) {
            l.add(this.w);
            for (Entry e : subentries) {
                l.addAll(e.match(""));
            }
        } else {
            for (Entry e : subentries) {
                l.addAll(e.match(keyword));
            }
        }
        return l;
    }

    public Widget getW() {
        return w;
    }

}
