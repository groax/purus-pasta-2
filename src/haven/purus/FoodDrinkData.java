package haven.purus;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import haven.Config;

public record FoodDrinkData(List<DrinkVessel> drinkVessels, List<FoodType> foodTypes, List<TypeDrink> typeDrinks,
                     List<VesselResource> vesselResources) {

    public FoodDrinkData() {
        this(List.of(), List.of(), List.of(), List.of());
    }

    public FoodDrinkData(List<DrinkVessel> drinkVessels, List<FoodType> foodTypes, List<TypeDrink> typeDrinks,
                         List<VesselResource> vesselResources) {
        this.drinkVessels = drinkVessels;
        this.foodTypes = foodTypes;
        this.typeDrinks = typeDrinks;
        this.vesselResources = vesselResources;
    }

    public static FoodDrinkData loadData() {
        var mapper = new ObjectMapper();
        try {
            return mapper.readValue(Config.loadJarFile("drink_data.json"), FoodDrinkData.class);
        } catch (Exception e) {
            e.printStackTrace();
            return new FoodDrinkData(List.of(), List.of(), List.of(), List.of());
        }
    }

    public record DrinkVessel(String drink, String vessel) {

    }

    public record FoodType(String name, List<String> types) {

    }

    public record TypeDrink(String type, List<String> drinks) {

    }

    public record VesselResource(String vessel, String resource) {

    }

}
