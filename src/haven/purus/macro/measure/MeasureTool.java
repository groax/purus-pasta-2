package haven.purus.macro.measure;

import haven.Area;
import haven.Coord;
import haven.MCache;
import haven.purus.pbot.api.PBotSession;
import haven.purus.pbot.api.PBotUtils.AreaReturn;
import haven.purus.pbot.api.callback.AreaSelectCallback;

public record MeasureTool(PBotSession session) implements AreaSelectCallback {

    public void start() {
        session.PBotUtils().selectArea(this);
    }

    @Override
    public void callback(AreaReturn obj) {
        Coord a = obj.getA().div(MCache.tilesz2);
        Coord b = obj.getB().div(MCache.tilesz2);
        Area area = new Area(a, b);
        double distance = a.dist(b);
        Coord size = area.sz();

        MeasureResultWindow resultWindow = new MeasureResultWindow(distance, size);
        session.getInternalGui().add(resultWindow);
    }

}
