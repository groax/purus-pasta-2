package haven.purus.macro.measure;

import haven.*;
import haven.purus.BetterWindow;

public class MeasureResultWindow extends BetterWindow {

    public MeasureResultWindow(double distance, Coord size) {
        super(UI.scale(200, 70), "Measurement result");
        String message = """
                Width: %s
                Height: %s
                Area: %s
                Distance: %.2f
                """.formatted(size.x, size.y, size.x * size.y, distance);
        add(new RichTextBox(UI.scale(200, 70), message));
    }

    @Override
    public void wdgmsg(Widget sender, String msg, Object... args) {
        if(sender == this) {
            reqdestroy();
        } else {
            super.wdgmsg(sender, msg, args);
        }
    }

}
