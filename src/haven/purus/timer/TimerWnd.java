package haven.purus.timer;

import java.io.Serial;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import haven.*;
import haven.purus.BetterWindow;
import haven.purus.Config;
import haven.purus.MultiSession;

public class TimerWnd extends BetterWindow {

	private TimerList vl;

	private static final Resource timerNotify = Resource.local().loadwait("sfx/alarms/timer");

	private static final SimpleDateFormat dfmt = new SimpleDateFormat("dd.MM HH:mm:ss");

	private static long server;
	private static long local;
	private static double delta;
	private static boolean serverTimeInitialized = false;

	public static void timerCalc() {
		for(Timer t : getTimersConfiguration()) {
			t.recalculateTimer();
		}
	}

	public static synchronized void updateServerTime(long time) {
		server = time;
		local = System.currentTimeMillis();
		serverTimeInitialized = true;
	}

	public static class TimerNotifier extends Window {
		public TimerNotifier(Timer t) {
			super(UI.scale(200, 60), "Timer completed!");
			adda(new Label("Timer " + t.name + " has finished."), UI.scale(100, 10), 0.5, 0.5);
			adda(new Button(UI.scale(60), "Ok"), UI.scale(100, 40), 0.5, 0.5)
			.action(this::reqdestroy);
		}
	}

	private static long changed = 1;
	private long changedLast = 0;

	public TimerWnd() {
		super(UI.scale(500, 300), "Timers");

		add(new Button(UI.scale(100), "Add timer"), UI.scale(5, 0))
		.action(() -> {
			Timer t = new Timer("timer", 0, null);
			vl.addItem(t);
			ArrayList<Timer> timersConfiguration = getTimersConfiguration();
			timersConfiguration.add(t);
            saveTimersConfiguration(timersConfiguration);
        });

		add(new Label("Hours"), UI.scale(345, 15));
		add(new Label("Minutes"), UI.scale(345 + 50, 15));
		add(new Label("Seconds"), UI.scale(345 + 100, 15));

		vl = new TimerList(UI.scale(500 - 15), 8);
		add(vl, UI.scale(15, 35));
	}

    public void refresh() {
		vl.clearItems();
		getTimersConfiguration().forEach(t -> vl.addItem(t));
	}


	@Override
	public void wdgmsg(Widget sender, String msg, Object... args) {
        if(sender == this) {
			hide();
		} else {
			super.wdgmsg(sender, msg, args);
		}
	}

	public static class Timer implements Serializable {
		@Serial
		private static final long serialVersionUID = 2047460725881980370L;
		private transient boolean initialized = false;
		public String name;
		public long seconds;
		public long minutes;
		public long hours;
		public long readyAt = 0;
		public Long startedAt;

		public Timer(String name, long duration, Long startedAt) {
			this.name = name;
			this.seconds = duration % 60;
			this.minutes = (duration % 3600) / 60;
			this.hours = duration / 3600;
			this.startedAt = startedAt;
		}

		public long duration() {
			return 1000 * (seconds + minutes * 60 + hours * 3600);
		}

		public long remaining() {
			return (readyAt - System.currentTimeMillis()) / 1000;
		}

		public synchronized void tick() {
			if (!initialized) {
				return;
			}
			if (startedAt == null || readyAt > System.currentTimeMillis()) {
				return;
			}
			Audio.play(timerNotify);
			startedAt = null;
			saveTimersConfiguration();
			changed++;
			MultiSession.activeSession.root.adda(new TimerNotifier(this), MultiSession.activeSession.root.sz.div(2),
												 0.5, 0.5);
		}

		public synchronized void recalculateTimer() {
			if (startedAt == null || !serverTimeInitialized) {
				return;
			}
			long now = System.currentTimeMillis();
			long remaining = (long) (duration() - now + local - (server - startedAt) / Glob.SERVER_TIME_RATIO);
			readyAt = System.currentTimeMillis() + remaining;
			initialized = true;
            tick();
        }
	}

	@Override
	public void tick(double dt) {
		delta += dt;
		if (delta < 0.2) {
			return;
		}
		delta = 0;
		List<TimerItem> items = new ArrayList<>(vl.items);
		items.stream().map(timerItem -> timerItem.timer).forEach(Timer::tick);
		if(changedLast != changed) {
			changedLast = changed;
			refresh();
		}
		super.tick(dt);
	}

    private synchronized static void saveTimersConfiguration() {
        Config.timersSet.setVal(getTimersConfiguration());
    }

    private synchronized static void saveTimersConfiguration(ArrayList<Timer> timers) {
        Config.timersSet.setVal(timers);
    }

    private synchronized static ArrayList<Timer> getTimersConfiguration() {
        return new ArrayList<>(Config.timersSet.val);
    }

	public class TimerList extends Widget {

		List<TimerItem> items = Collections.synchronizedList(new ArrayList<>());
		Scrollbar sb;
		int rowHeight = UI.scale(30);
		int rows, w;
		List<TimerItem> visibleItems = Collections.synchronizedList(new ArrayList<>());

		public TimerList(int w, int rows) {
			super(UI.scale(w, 30 * rows));
			this.rows = rows;
			this.w = w;
			sb = new Scrollbar(rowHeight * rows, 0, 100) {
				@Override
				public void changed() {
					updShow();
					super.changed();
				}
			};
			add(sb, UI.scale(0, 0));
		}

		public void updShow() {
			for(TimerItem ti : visibleItems) {
				ti.remove();
			}
			visibleItems.clear();
			sb.max = items.size()-rows;
			for(int i=0; i<rows; i++) {
				if(i+sb.val >= items.size())
					break;
				visibleItems.add(items.get(i+sb.val));
				add(items.get(i+sb.val), new Coord(UI.scale(15), i*rowHeight));
				items.get(i+sb.val).show();
			}
		}

		public TimerItem listitem(int i) {
			return items.get(i);
		}

		public void addItem(Timer t) {
			items.add(new TimerItem(t, this.w));
			updShow();
		}

		public void clearItems() {
			this.children().forEach(w -> {if(w instanceof TimerItem)w.destroy();});
			visibleItems.clear();
			items.clear();
		}

		public int listitems() {
			return items.size();
		}

		@Override
		public boolean mousewheel(Coord c, int amount) {
			sb.ch(amount);
			return true;
		}

	}
	public class TimerItem extends Widget {

		private final Timer timer;
		private Label remaining;

		public TimerItem(Timer timer, int w) {
			super(UI.scale(w, 30));
			this.timer = timer;
			int x = 0;
			add(new TextEntry(UI.scale(200), timer.name) {
				@Override
				protected void changed() {
					timer.name = this.buf.line();
                    saveTimersConfiguration();
                    super.changed();
				}
			}, UI.scale(x, 5));
			x += 200 + 5;
			if(timer.startedAt == null) {
				add(new Button(UI.scale(50), "Start"), UI.scale(x, 5))
						.action(() -> {
							timer.startedAt = (long) (server + Glob.SERVER_TIME_RATIO * (System.currentTimeMillis() - local));
							timer.recalculateTimer();
							saveTimersConfiguration();
							changed++;
						});
			} else {
				add(new Button(UI.scale(50), "Stop"), UI.scale(x, 5))
						.action(() -> {
							timer.startedAt = null;
							timer.recalculateTimer();
							saveTimersConfiguration();
							changed++;
						});
			}
			x +=  50 + 5;
			add(new Button(UI.scale(50), "Delete") {
				@Override
				public void click() {
					try {
						timer.startedAt = null;
						ArrayList<Timer> timersConfiguration = getTimersConfiguration();
						timersConfiguration.remove(timer);
						saveTimersConfiguration(timersConfiguration);
						changed++;
						super.click();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}, UI.scale(x, 5));
			x +=  50 + 5;
			if(timer.startedAt != null) {
				remaining = add(new Label(String.format("%02d : %02d : %02d", timer.remaining() / 3600, timer.remaining() % 3600 / 60, timer.remaining() % 60)), UI.scale(x, 5));
			} else {
				add(new TextEntry(UI.scale(45), Long.toString(timer.hours)) {
					@Override
					protected void changed() {
						try {
							timer.hours = Long.parseLong(this.buf.line());
                            saveTimersConfiguration();
                            changed++;
						} catch(NumberFormatException e) {
						}
						super.changed();
					}
				}, UI.scale(x, 5));
				x += 50;
				add(new TextEntry(UI.scale(45), Long.toString(timer.minutes)) {
					@Override
					protected void changed() {
						try {
							timer.minutes = Long.parseLong(this.buf.line());
                            saveTimersConfiguration();
                            changed++;
						} catch(NumberFormatException e) {
						}
						super.changed();
					}
				}, UI.scale(x, 5));
				x += 50;
				add(new TextEntry(UI.scale(45), Long.toString(timer.seconds)) {
					@Override
					protected void changed() {
						try {
							timer.seconds = Long.parseLong(this.buf.line());
                            saveTimersConfiguration();
                            changed++;
						} catch(NumberFormatException e) {
						}
						super.changed();
					}
				}, UI.scale(x, 5));
			}
		}

		@Override
		public void draw(GOut g) {
			Long startedAt = timer.startedAt;
			if(startedAt != null && remaining != null) {
				remaining.settext(String.format("%02d : %02d : %02d", timer.remaining() / 3600, timer.remaining() % 3600 / 60, timer.remaining() % 60) + "  " + dfmt.format(new Date(System.currentTimeMillis() + timer.remaining() * 1000)));
			}
			super.draw(g);
		}
	}
}
