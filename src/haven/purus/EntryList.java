package haven.purus;

import java.util.ArrayList;
import java.util.List;

import haven.*;

public class EntryList extends Widget {

    Entry root = new Entry(add(new Widget()), "*");
    List<Widget> filteredWidgets = new ArrayList<>();

    private Scrollbar sb;

    public EntryList(Coord sz) {
        super(sz);
        sb = add(new Scrollbar(sz.y, 0, 0), sz.x, 0);
        sb.resize(sz.y);
        sb.show();
        sb.c = new Coord(sz.x - sb.sz.x, 0);
    }

    public void search(String s) {
        filteredWidgets = root.match(s);
        this.sb.max = 0;
        for (Widget w : filteredWidgets) {
            this.sb.max += w.sz.y + UI.scale(15);
            if (w.parent == null) {
                add(w);
            }
        }
        for (Widget w : this.children(Widget.class)) {
            w.hide();
        }
        this.sb.max -= this.sz.y;
        if (this.sb.max < 0) {
            this.sb.hide();
        } else {
            this.sb.show();
        }
        this.sb.val = 0;
    }

    public void draw(GOut g) {
        int curY = 0;
        for (Widget w : filteredWidgets) {
            if (curY + w.sz.y > sb.val) {
                w.c = new Coord(10, curY - sb.val);
                w.visible = true;
            } else {
                w.visible = false;
            }
            curY += w.sz.y + UI.scale(15);
        }
        super.draw(g);
    }

    @Override
    public boolean mousedown(Coord c, int button) {
        int curY = 0;
        for (Widget w : filteredWidgets) {
            if (curY - sb.val > sz.y) {
                break;
            }
            if (curY + w.sz.y > sb.val) {
                Coord cc = xlate(new Coord(10, curY - sb.val), true);
                if (c.isect(cc, w.sz)) {
                    if (w.mousedown(c.add(cc.inv()), button)) {
                        return (true);
                    }
                }
            }
            curY += w.sz.y + UI.scale(15);
        }
        return super.mousedown(c, button);
    }

    @Override
    public boolean mousewheel(Coord c, int amount) {
        sb.ch(amount * UI.scale(50));
        return true;
    }

}
