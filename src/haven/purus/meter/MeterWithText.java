package haven.purus.meter;

import java.util.List;

import haven.*;
import haven.purus.Config;

public class MeterWithText extends IMeter {

    private Tex meterText;

    public MeterWithText(Indir<Resource> bg, List<Meter> meters) {
        super(bg, meters);
    }

    @Override
    public void draw(GOut g) {
        super.draw(g);
        if (meterText != null && Config.meterText.val) {
            g.aimage(meterText, sz.div(2).addy(-1), 0.4f, 0.5f);
        }
    }

    @Override
    public void uimsg(String msg, Object... args) {
        super.uimsg(msg, args);
        if(msg.equals("tip")) {
            final String tt = (String) args[0];
            this.meterText = Text.renderStroked(tt.substring(tt.indexOf(':') + 1)).tex();
        }
    }

}
