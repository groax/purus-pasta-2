package haven;

import java.awt.*;
import java.util.HashSet;

import haven.Gob.Knocked;
import haven.Gob.Overlay;
import haven.Gob.Type;
import haven.purus.Config;
import haven.purus.*;
import haven.purus.alarms.AlarmManager;
import haven.render.MixColor;

class GobCustomization implements Runnable {

    private final Gob gob;

    private static final MixColor dFrameDone = new MixColor(255, 0, 0, 64);
    private static final MixColor dFrameEmpty = new MixColor(0, 255, 0, 64);
    private static final MixColor ttEmpty = new MixColor(255, 0, 0, 64);
    private static final MixColor ttDone = new MixColor(0, 255, 0, 64);

    private static final HashSet<Long> alarmPlayed = new HashSet<Long>();

    public GobCustomization(Gob gob) {
        this.gob = gob;
    }

    @Override
    public void run() {
        try {
            Resource res = gob.getres();
            if (res != null) {
                if (!alarmPlayed.contains(gob.id) || Config.playAlarmOnReappear.val) {
                    if (AlarmManager.play(res.name, gob)) {
                        alarmPlayed.add(gob.id);
                    }
                    if (gob.type == Type.PLAYER && gob.id != gob.glob.sess.ui.gui.plid) {
                        KinInfo kin = gob.getattr(KinInfo.class);
                        if (kin == null) {
                            Audio.play(Resource.local().loadwait("sfx/alarms/whitePlayer"));
                            alarmPlayed.add(gob.id);
                        } else if (kin.group == 2) {
                            alarmPlayed.add(gob.id);
                            Audio.play(Resource.local().loadwait("sfx/alarms/redPlayer"));
                        }
                    }
                }
                if (res.name.startsWith("gfx/kritter")) {
                    processKritterRadius(res);
                } else if (res.name.startsWith("gfx/borka/body")) {
                    processPlayerWarning();
                }
                String resname = res.name;
                if (haven.purus.Config.bbDisplayState.val > 0) {
                    BoundingBox bb = BoundingBox.getBoundingBox(gob);
                    Overlay ol = gob.findol(1339);
                    if (ol != null && bb == null) {
                        ol.remove(false);
                    } else if (ol == null && bb != null) {
                        gob.addol(new Overlay(gob, new GobBoundingBox(gob, bb), 1339), false);
                    }
                }
                Drawable d = gob.getattr(Drawable.class);
                processHiding(d, res);
                ResDrawable rd = gob.getattr(ResDrawable.class);
                if (haven.purus.Config.ttfHighlight.val) {
                    if (res.name.equals("gfx/terobjs/dframe")) {
                        colorDryingFrame();
                    } else if (res.name.equals("gfx/terobjs/ttub")) {
                        colorTanningTube(rd);
                    } else if (res.name.equals("gfx/terobjs/beehive")) {
                        colorBeeHive(rd);
                    }
                } else {
                    gob.delattr(GobColor.class);
                }

                if (Config.growthStages.val) {
                    if (resname.startsWith("gfx/terobjs/bushes") ||
                        (resname.startsWith("gfx/terobjs/trees") && !resname.endsWith("log") &&
                         !resname.endsWith("oldtrunk"))) {
                        processTreeStage(rd);
                    } else if (resname.startsWith("gfx/terobjs/plants") && !resname.endsWith("trellis") && rd != null) {
                        processPlantStage(rd, res, resname);
                    }
                }
            }
        } catch (Loading l) {
            l.waitfor(this, waiting -> {
            });
        }
    }

    private void processTreeStage(ResDrawable rd) {
        if (rd != null) {
            int fscale = rd.sdt.peekrbuf(1);
            if (fscale != -1) {
                Overlay ovl = gob.findol(1337);
                synchronized (this) {
                    if (ovl == null) {
                        gob.addol(new Overlay(gob, new GobText(gob, fscale + "%", Color.WHITE, 5), 1337), false);
                    } else if (!((GobText) ovl.spr).text.equals(fscale + "%")) {
                        ovl.remove(false);
                        gob.addol(new Overlay(gob, new GobText(gob, fscale + "%", Color.WHITE, 5), 1337), false);
                    }
                }
            }
        }
    }

    private void processPlantStage(ResDrawable rd, Resource res, String resname) {
        int stage = rd.sdt.peekrbuf(0);
        if (gob.cropstgmaxval == 0) {
            for (FastMesh.MeshRes layer : res.layers(FastMesh.MeshRes.class)) {
                int stg = layer.id / 10;
                if (stg > gob.cropstgmaxval) {
                    gob.cropstgmaxval = stg;
                }
            }
        }
        Overlay ol = gob.findol(1338);
        String text;
        Color col;
        if (resname.endsWith("/fallowplant")) {
            col = Color.GRAY;
            text = "-1";
        } else if (stage == gob.cropstgmaxval) {
            col = Color.GREEN;
            text = stage + "/" + gob.cropstgmaxval;
        } else if (stage == 0) {
            col = Color.RED;
            text = stage + "/" + gob.cropstgmaxval;
        } else {
            col = Color.YELLOW;
            text = stage + "/" + gob.cropstgmaxval;
        }
        synchronized (this) {
            if (ol == null) {
                gob.addol(new Overlay(gob, new GobText(gob, text, col, -4), 1338), false);
            } else if (!((GobText) ol.spr).text.equals(text)) {
                ol.remove(false);
                gob.addol(new Overlay(gob, new GobText(gob, text, col, -4), 1338), false);
            }
        }
    }

    private void colorTanningTube(ResDrawable rd) {
        if (rd == null) {
            return;
        }
        int r = rd.sdt.peekrbuf(0);
        if ((r & (0x8)) == 0x8) {
            gob.glob.loader.defer(() -> {
                synchronized (gob) {
                    gob.setattr(new GobColor(gob, ttDone));
                }
            }, null);
        } else if ((r & (0x4)) == 0 || r == 5) {
            gob.glob.loader.defer(() -> {
                synchronized (gob) {
                    gob.setattr(new GobColor(gob, ttEmpty));
                }
            }, null);
        } else {
            gob.glob.loader.defer(() -> {
                synchronized (gob) {
                    gob.delattr(GobColor.class);
                }
            }, null);
        }
    }

    private void colorBeeHive(ResDrawable rd) {
        if (rd == null) {
            return;
        }
        int r = rd.sdt.peekrbuf(0);
        if ((r & (0x4)) == 0x4) {
            gob.glob.loader.defer(() -> {
                synchronized (gob) {
                    gob.setattr(new GobColor(gob, ttDone));
                }
            }, null);
        } else {
            gob.glob.loader.defer(() -> {
                synchronized (gob) {
                    gob.delattr(GobColor.class);
                }
            }, null);
        }
    }

    private void colorDryingFrame() {
        boolean done = true;
        boolean empty = true;
        for (Overlay ol : gob.ols) {
            if (ol.res != null) {
                Resource olres = ol.res.get();
                if (olres.name.endsWith("-blood") || olres.name.endsWith("-windweed") ||
                    olres.name.endsWith("-fishraw")) {
                    done = false;
                }
                if (olres.name.startsWith("gfx/terobjs/dframe-")) {
                    empty = false;
                }
            }
        }
        synchronized (this) {
            if (empty) {
                gob.glob.loader.defer(() -> {
                    synchronized (gob) {
                        gob.setattr(new GobColor(gob, dFrameEmpty));
                    }
                }, null);
            } else if (done) {
                gob.glob.loader.defer(() -> {
                    synchronized (gob) {
                        gob.setattr(new GobColor(gob, dFrameDone));
                    }
                }, null);
            } else {
                gob.glob.loader.defer(() -> {
                    synchronized (gob) {
                        gob.delattr(GobColor.class);
                    }
                }, null);
            }
        }
    }

    private void processHiding(Drawable d, Resource res) {
        boolean hide = false;
        if (Config.hideToggle.val && d != null) {
            if (Config.hideTrees.val && res.name.startsWith("gfx/terobjs/trees") && !res.name.endsWith("log") &&
                !res.name.endsWith("oldtrunk")) {
                hide = true;
            } else if (Config.hideHouses.val && (res.name.endsWith("/stonemansion") || res.name.endsWith("/logcabin") ||
                                                 res.name.endsWith("/greathall") || res.name.endsWith("/stonestead") ||
                                                 res.name.endsWith("/timberhouse") || res.name.endsWith("stonetower") ||
                                                 res.name.endsWith("windmill"))) {
                hide = true;
            } else if (Config.hideWalls.val && res.name.startsWith("gfx/terobjs/arch/pali") &&
                       !res.name.equals("gfx/terobjs/arch/palisadegate") &&
                       !res.name.equals("gfx/terobjs/arch/palisadebiggate") ||
                       res.name.startsWith("gfx/terobjs/arch/brick") &&
                       !res.name.equals("gfx/terobjs/arch/brickwallgate") &&
                       !res.name.equals("gfx/terobjs/arch/brickwallbiggate") ||
                       res.name.startsWith("gfx/terobjs/arch/pole") && !res.name.equals("gfx/terobjs/arch/polegate") &&
                       !res.name.equals("gfx/terobjs/arch/polebiggate")) {
                hide = true;
            } else if (Config.hideBushes.val && res.name.startsWith("gfx/terobjs/bushes")) {
                hide = true;
            } else if (Config.hideCrops.val && res.name.startsWith("gfx/terobjs/plants") &&
                       !res.name.endsWith("trellis")) {
                hide = true;
            }
        }
        if (d != null && d.hide != hide) {
            d.hide = hide;
            gob.glob.loader.defer(() -> {
                synchronized (gob) {
                    gob.setattr(d);
                }
            }, null);
            Overlay ol = gob.findol(1340);
            if (!hide && ol != null) {
                ol.remove(false);
            } else if (hide && ol == null) {
                BoundingBox bb = BoundingBox.getBoundingBox(gob);
                gob.addol(new Overlay(gob, new GobHideBox(bb), 1340), false);
            }
        }
    }

    private void processPlayerWarning() {
        Overlay ol = gob.findol(1342);
        if (Config.playerRadiuses.val && !gob.isPlayer()) {
            if (ol == null) {
                KinInfo kin = gob.getattr(KinInfo.class);
                if (kin == null) {
                    gob.addol(new Overlay(gob, new PlayerRad(gob, null, Color.WHITE), 1342), false);
                } else {
                    gob.addol(new Overlay(gob, new PlayerRad(gob, null, BuddyWnd.gc[kin.group]), 1342), false);
                }
                gob.updstate();
            }
        } else if (ol != null) {
            ol.remove(false);
        }
    }

    private void processKritterRadius(Resource res) {
        Overlay ol = gob.findol(1341);
        if (!Config.animalRads.val.containsKey(res.name)) {
            Config.animalRads.val.put(res.name, true);
            Config.animalRads.setVal(Config.animalRads.val);
        }
        if (Config.animalRadiuses.val && Config.animalRads.val.getOrDefault(res.name, true) &&
            gob.knocked != Knocked.TRUE) {
            if (ol == null) {
                gob.addol(new Overlay(gob, new AnimalRad(gob, null, 5 * MCache.tilesz2.y), 1341), false);
            }
        } else if (ol != null) {
            ol.remove(false);
        }
    }

}
