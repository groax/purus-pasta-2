package haven;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import haven.GobIcon.Setting;
import haven.GobIcon.Settings;

public class CustomIcons {

    private static final List<CustomIconDefinition> DEFINITIONS = List.of(
            new CustomIconDefinition("gfx/terobjs/items/arrow", "gfx/invobjs/arrow", true),
            new CustomIconDefinition("gfx/terobjs/vehicle/wheelbarrow", "paginae/bld/wheelbarrow", false),
            new CustomIconDefinition("gfx/terobjs/vehicle/rowboat", "paginae/bld/rowboat", true),
            new CustomIconDefinition("gfx/terobjs/vehicle/snekkja", "paginae/bld/snekkja", true),
            new CustomIconDefinition("gfx/terobjs/vehicle/knarr", "paginae/bld/knarr", true),
            new CustomIconDefinition("gfx/terobjs/vehicle/dugout", "paginae/bld/dugout", false),
            new CustomIconDefinition("gfx/terobjs/items/tinkersthrowingaxe", "gfx/invobjs/tinkersthrowingaxe", true),
            new CustomIconDefinition("gfx/terobjs/vehicle/plow", "paginae/bld/metalplow", false),
            new CustomIconDefinition("gfx/terobjs/vehicle/wagon", "paginae/bld/wagon", false),
            new CustomIconDefinition("gfx/terobjs/pclaim", "paginae/bld/claim", false),
            new CustomIconDefinition("gfx/terobjs/villageidol", "paginae/bld/vilgate", false),
            new CustomIconDefinition("gfx/terobjs/minehole", "paginae/bld/minehole", false));

    private static final Map<String, CustomIconDefinition> GOB_DEFINITIONS = DEFINITIONS.stream()
                                                                                        .collect(Collectors.toMap(
                                                                                                CustomIconDefinition::gobResourceName,
                                                                                                Function.identity()));

    public static void loadCustomIcons(Settings settings) {
        DEFINITIONS.stream()
                   .filter(definition -> !settings.settings.containsKey(definition.inventoryResourceName()))
                   .forEach(definition -> {
                       insertCustomIcon(definition, settings);
                   });
    }

    private static void insertCustomIcon(CustomIconDefinition definition, Settings settings) {
        Resource.Spec res = new Resource.Spec(null, definition.inventoryResourceName(), 1);
        Setting setting = new Setting(res);
        setting.show = definition.defaultVisible();
        settings.settings.put(definition.inventoryResourceName(), setting);
    }

    public static void processGob(Gob gob) {
        String name = Optional.ofNullable(gob).map(Gob::getres).map(g -> g.name).orElse("");
        CustomIconDefinition definition = GOB_DEFINITIONS.get(name);
        if (definition == null || gob == null || gob.getattr(GobIcon.class) != null) {
            return;
        }

        Resource.Spec res = new Resource.Spec(Resource.remote(), definition.inventoryResourceName());
        gob.setattr(new GobIcon(gob, res));
    }

    private record CustomIconDefinition(String gobResourceName, String inventoryResourceName, boolean defaultVisible) {

    }

}
