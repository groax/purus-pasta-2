package example


import example.subdir.SubClassTest
import haven.purus.pbot.api.PBotSession
import haven.purus.pbot.external.HnhBot

class ExampleBot extends HnhBot {

    @Override
    void execute(PBotSession botSession) {
        botSession.PBotUtils().sysMsg("Script starts running")
        def testClass = new SubClassTest()
        testClass.printSomething(botSession)
        botSession.PBotUtils().sysMsg("Example script finished successfully")
    }

}
