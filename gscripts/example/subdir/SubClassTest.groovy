package example.subdir

import haven.purus.pbot.api.PBotSession

class SubClassTest {

    void printSomething(PBotSession botSession) {
        botSession.PBotUtils().sysMsg("SubClass worked");
    }

}
